package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Biodata;

@Controller
public class BiodataController {
	
	@RequestMapping("/biodata/index")
	public String index() {
		return "biodata/index";
	}
	
	@RequestMapping("biodata/add")
	public String add() {
		return "biodata/add";
	}
	
	@RequestMapping(value="/biodata/save", method=RequestMethod.POST)
	public String save(@ModelAttribute Biodata item, Model model) {
		model.addAttribute("data",item);
		return "biodata/save";
	}
}