package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Cars;

@Controller
public class CarsController {
	
	@RequestMapping("/cars/index")
	public String index() {
		return "cars/index";
	}
	
	@RequestMapping("cars/add")
	public String add() {
		return "cars/add";
	}
	
	@RequestMapping(value="/cars/save", method=RequestMethod.POST)
	public String save(@ModelAttribute CarsController item, Model model) {
		model.addAttribute("data", item);
		return "cars/save";
	}
}
