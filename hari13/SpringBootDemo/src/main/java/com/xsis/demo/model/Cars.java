package com.xsis.demo.model;

public class Cars {

	private int id;
	private String nama;
	private String tipe;
	private String besarcc;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id=id;
	}
	
	public String getNama() {
		return nama;
	}
	
	public void setNama(String nama) {
		this.nama=nama;
	}
	
	public String getTipe() {
		return tipe;
	}
	
	public void setTipe(String tipe) {
		this.tipe=tipe;
	}
	
	public String getBesarCC() {
		return besarcc;
	}
	
	public void setBesarCC(String besarcc) {
		this.besarcc=besarcc;
	}
	
}
