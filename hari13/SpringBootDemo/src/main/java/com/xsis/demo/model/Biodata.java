package com.xsis.demo.model;

public class Biodata {
	private int id;
	private String nama;
	private String alamat;
	private String jk;
	private String telp;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id=id;
	}
	
	public String getNama() {
		return nama;
	}
	
	public void setNama(String nama) {
		this.nama=nama;
	}
	
	public String getAlamat() {
		return alamat;
	}
	
	public void setAlamat(String alamat) {
		this.alamat=alamat;
	}
	
	public String getJk() {
		return jk;
	}
	
	public void setJk(String jk) {
		this.jk=jk;
	}
	
	public String getTelp() {
		return telp;
	}
	
	public void setTelp(String telp) {
		this.telp=telp;
	}
}
