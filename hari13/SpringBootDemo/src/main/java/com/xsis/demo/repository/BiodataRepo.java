package com.xsis.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.demo.model.Biodata;

@Repository
public interface BiodataRepo extends JpaRepository<Biodata, Integer> {

}