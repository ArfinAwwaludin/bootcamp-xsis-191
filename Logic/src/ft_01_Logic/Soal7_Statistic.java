package ft_01_Logic;

import java.util.Arrays;

public class Soal7_Statistic {

	//membuat method
	public static void statistic(int[] arr) {
		
		//deklarasi variabel total untuk menghitung jumlah seluruh komponen array
		int total = 0;
		
		//looping utk dapetin total seluruh komponen array dan digunakan untuk perhitungan mean
		for (int i = 0; i < arr.length; i++) {
			//total seluruh komponen array
			total = total + arr[i];
		}
		
		//looping utk dapetin nilai modus
		int[] type = new int[9];
		
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < type.length; j++) {
				if(arr[i] == j) {
					type[j]++;
				}
			}
		}
		
		int max = type[0];
		int modus = 0;
		
		for (int k = 0; k < type.length; k++) {
			if(type[k] > max) {
				max = type[k];
				modus = k;
			}
		}
		
		//perhitungan untuk mean dan median
		int mean = total/arr.length;
		int median = arr[arr.length/2];
		
		System.out.println(mean);
		System.out.println(median);
		System.out.println(modus);
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int[] arr = {8,7,0,2,7,-1,7,6,3,0,7,-1,3,4,6,-1,6,4,3,0,0,-1};
		
		//membuat komponen array menjadi urut
		Arrays.sort(arr);
		
		System.out.println(Arrays.toString(arr));
		
		//output
		statistic(arr);
	}
	
	
}
