package ft_01_Logic;

import java.util.Arrays;
import java.util.List;

public class Soal8_MinAndMax {

	//membuat method
	public static void minAndMax(int[] input) {
		
		int total = 0;
		
		for (int i = 0; i < input.length; i++) {
			//bikin perhitungan utk dapetin nilai total semua value pada array
			total = total + input[i];	
		}
		
		//looping utk ngejumlah nilai minimum (4 angka awal)
		int totalMin = 0;
		for (int i = 0; i < 4; i++) {
			totalMin = totalMin + input[i];
		}
		
		//looping utk negjumlah nilai maksimum (4 angka terakhir)
		int totalMax = 0;
		for (int i = input.length; i < input.length-4; i--) {
			totalMax = totalMax + input[i];
		}
		
		System.out.println("min : " +(total-totalMax));
		System.out.println("max : " +(total-totalMin));
		
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int[] input = {1,2,4,7,8,6,9};
		
		//mengurutkan array
		Arrays.sort(input);
		
		//memanggil method dan menampilkan hasil
		minAndMax(input);
		
	}
}
