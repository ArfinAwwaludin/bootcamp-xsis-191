package ft_01_Logic;

import java.util.Scanner;

public class Soal2_Palindrome {
	
	public static void main(String[] args) {
		
		//deklarasi variabel string kata asli dan kata balik
		String kataAsli = "";
		String kataBalik = "";
		
		//memanggil Scanner
		Scanner scn = new Scanner(System.in);
		System.out.print("inputkan kata : ");
		
		//mendeklarasikan variabel kataAsli utk menampung inputan yg telah dibuat
		kataAsli = scn.nextLine();
		
		//deklarasi variabel panjang
		int panjang = kataAsli.length();
		
		//looping untuk mengecek tiap indeks array
		for (int i = panjang-1; i >= 0; i--) {
			kataBalik = kataBalik + kataAsli.charAt(i);	
		}
		
		//if-else statement untuk output
		if (kataAsli.equals(kataBalik)) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
	}
}
