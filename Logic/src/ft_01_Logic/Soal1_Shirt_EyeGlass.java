package ft_01_Logic;

import java.util.Scanner;

public class Soal1_Shirt_EyeGlass { 
	
	//main method
	public static void main(String[] args) {
		
		//membuat scanner utk inputan
		Scanner in = new Scanner(System.in);
		
		//variabel utk menampung inputan
        int amount = in.nextInt();
        int shirtCount = in.nextInt();
        int eyeGlassCount = in.nextInt();
        
        //deklarasi utk array shirt dan eyeglass
        int[] shirts = new int[shirtCount]; 
        int[] eyeGlass = new int[eyeGlassCount];
        
        //looping utk shirt
        for (int i = 0; i < shirtCount; i++) {
			shirts[i] = in.nextInt();
		}
        
        //looping utk eyeglass
        for (int i = 0; i < eyeGlassCount; i++) {
			eyeGlass[i] = in.nextInt();
		}
        
        //looping dan if statment utk mendapatkan nilai optimum
        int spentAmount = -1;
        for (int i = 0; i < shirtCount; i++) {
			for (int j = 0; j < eyeGlassCount; j++) {
				int sum = shirts[i] + eyeGlass[j];
				if (sum <= amount && sum > spentAmount) {
					spentAmount = sum;
				}
			}
		}
        System.out.println(spentAmount);
	}
}
