package repeatedExercise;

/*
 * Complete the function solveMeFirst to compute the sum of two integers.
 * Function prototype:
  	int solveMeFirst(int a, int b);
 * where,
   a is the first integer input.
   b is the second integer input
 * Return values
   sum of the above two integers
 */

public class HR_Soal1_SolveMeFirst_Ulang1 {
	
	public static int solveMeFirst(int a, int b) {
		int c = a+b;
		return c;
	}
	
	public static void main(String[] args) {
		//inputan
		int a = 1;
		int b = 2;
		int hasil = solveMeFirst(a, b);
		
		System.out.println(hasil);
	}

}
