package repeatedExercise;

public class HR_DiagonalDifference_Ulang4 {
	
	//membuat method
	public static int diagonalDifference(int [][] arr) {
		//deklarasi variabel
		int a = 0;
		int b = 0;
		
		//looping utk array
		for (int i = 0; i < arr.length; i++) {
			a += arr[i][i];
			a += arr[i][arr.length-1-i];
		}
		
		//return method
		if (a>b) {
			return a-b;
		} else {
			return b-a;
		}
	}

	public static void main(String[] args) {
		//inputan
		int[][] arr = {{1,2,3},{4,5,6},{7,8,9}};
		
		//memanggil method dan menampilkan hasil
		System.out.println(diagonalDifference(arr));

	}

}
