package repeatedExercise;

import java.util.Arrays;

public class HR_Soal10_CompareTheTriplets_Ulang2 {
	
	//membuat method
	public static int[] compareTheTriplets(int[] a, int[] b) {
		
		//deklarasi var penampung jumlah cacah alice dan bob
		int nAlice = 0;
		int nBob = 0;
		
		//looping utk array
		for (int i = 0; i < a.length; i++) {
			if (a[i]>b[i]) {
				nAlice++;
			} else if(a[i]<b[i]) {
				nBob++;
			}
		}
		
		//bikin array utk nampung hasil
		int[] hasil = new int[2];
		hasil[0] = nAlice;
		hasil[1] = nBob;
		
		//return method
		return hasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int[] a = {4,5,6};
		int[] b = {5,5,3};
		
		//manggil method dan nampilin output
		System.out.println(Arrays.toString(compareTheTriplets(a, b)));
	}

}
