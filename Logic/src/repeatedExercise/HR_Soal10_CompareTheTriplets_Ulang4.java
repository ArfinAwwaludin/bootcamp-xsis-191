package repeatedExercise;

import java.util.ArrayList;
import java.util.List;

public class HR_Soal10_CompareTheTriplets_Ulang4 {
	//membuat method
	public static List<Integer> compareTheTriplets(List<Integer> a, List<Integer> b){
		
		//deklarasi variabel 
		int nAlice = 0;
		int nBob = 0;
		
		//looping utk array
		for (int i = 0; i < a.size(); i++) {
			//if else statement
			if (a.get(i)>b.get(i)) {
				nAlice++;
			} else if(a.get(i)<b.get(i)){
				nBob++;
			}
		}
		
		//deklarasi utk return
		List<Integer> hasil = new ArrayList<>();
		hasil.add(nAlice);
		hasil.add(nBob);
		
		//return method
		return hasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		List<Integer> a = new ArrayList<>();
		a.add(3);
		a.add(7);
		a.add(6);
		
		List<Integer> b = new ArrayList<>();
		b.add(6);
		b.add(8);
		b.add(1);
		
		//memanggil method dan menampilkan output
		System.out.println(compareTheTriplets(a, b));
	}

}
