package repeatedExercise;

public class HR_Soal21_UtopianTree_Ulang1 {
	static int utopianTree(int n) {
		int h=1;
		for (int i = 0; i < n; i++) {
			if(i%2==0) {
				h*=2;
			} else {
				h++;
			}
		}
		return h;
	}
	public static void main(String[] args) {
		System.out.println(utopianTree(5));
	}
}
