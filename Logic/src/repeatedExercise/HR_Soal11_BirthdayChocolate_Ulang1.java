package repeatedExercise;

public class HR_Soal11_BirthdayChocolate_Ulang1 {
	
	//membuat method
	public static int birthdayChocolate(int d, int m, int[] arr) {
		//deklarasi variabel utk menampung jumlah kemungkinan
		int nHasil = 0;
		
		//looping utk array inputan
		for (int i = 0; i < arr.length-m; i++) {
			
			//deklarasi variabel total utk menampung dan membandingkan nilainya dengan d 
			int  total = 0;
			
			//looping sebanyak m
			for (int j = 0; j < m; j++) {
				total = total + arr[i+j];
			}	
			
			//if statement utk nambahin nHasil kalo total == d
			if (total == d) {
				nHasil++;
			}
		}
		//return method
		return nHasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int[] arr = {1,2,1,3,2};
		int d = 3;
		int m = 2;
		
		//memanggil method dan output
		System.out.println(birthdayChocolate(d, m, arr));
	}

}
