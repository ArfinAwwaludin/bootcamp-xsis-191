package repeatedExercise;

import java.util.ArrayList;
import java.util.List;

public class HR_Soal11_BirthdayChocolate_Ulang4 {
	//ngerjain tapi pake list array
	
	//bikin method
	public static int birthdayChocolate(int m, int d, List<Integer> arr) {
		//deklarasi var utk nampung cacah
		int hasil = 0;
		
		//looping utk array
		for (int i = 0; i < arr.size()-m; i++) {
			//deklarasi var total utk nampung nilai utk dibanding
			int total = 0;
			
			//looping sebanyak m
			for (int j = 0; j < m; j++) {
				total = total + arr.get(i+j);
			}
			
			//if statement utk cek total dgn nilai d
			if (total == d) {
				hasil++;
			}
		}
		//return method
		return hasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int m = 2;
		int d = 2;
		List<Integer> arr = new ArrayList<>();
		arr.add(1);
		arr.add(1);
		arr.add(1);
		arr.add(1);
		arr.add(1);
		arr.add(1);
		
		//manggil method dan nampilin hasil
		System.out.println(birthdayChocolate(m, d, arr));
	}
}
