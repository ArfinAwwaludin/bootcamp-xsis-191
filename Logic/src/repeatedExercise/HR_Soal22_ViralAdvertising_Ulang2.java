package repeatedExercise;

public class HR_Soal22_ViralAdvertising_Ulang2 {
	//bikin method
	public static int viralAdvertising(int n) {
		
		//inisialisasi nilai awal utk like, share dan kumulatif
		int share = 5;
		int like = 0;
		int cumulative = 0;
		
		//looping utk ngitung jumlah like di hari ke-n
		for (int i = 0; i < n; i++) {
			like = share/2;
			cumulative = cumulative+like;
			share = like*3;
		}
		
		return cumulative;
		
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int n = 4;
		
		//manggil method dan nampilin hasil
		System.out.println(viralAdvertising(n));
	}
	
}
