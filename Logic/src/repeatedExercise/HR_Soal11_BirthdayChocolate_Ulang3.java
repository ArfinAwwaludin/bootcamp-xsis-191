package repeatedExercise;

public class HR_Soal11_BirthdayChocolate_Ulang3 {
	
	//membuat method
	public static int birthdayChocolate(int m, int d, int[] arr) {
		//deklarasi var utk nampung jmlh cacar kemungkinan
		int hasil = 0;
		
		//looping utk array
		for (int i = 0; i < arr.length-m; i++) {
			//deklarasi variabel total utk nampung nilai dan ngebandingin dgn nilai d
			int total = 0;
			
			//looping sebanyak m
			for (int j = 0; j < m; j++) {
				total = total+arr[i+j];
			}
			
			if (total == d) {
				hasil++;
			}
		}
		
		//return method
		return hasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int m = 2;
		int d = 2;
		int[] arr = {1,1,1,1,1,1};
		
		//manggil method dan nampilin output
		System.out.println(birthdayChocolate(m, d, arr));
	}

}
