package repeatedExercise;

public class HR_Soal12_DivisibleSumPairs_Ulang2 {

	//bikin method
	public static int divisibleSumPairs(int k, int[] arr) {
		//deklarasi var penampung cacah kemungkinan
		int hasil = 0;
		
		//looping utk array
		for (int i = 0; i < arr.length; i++) {
			//looping kembali utk ngebandingin dgn indeks2 setelahnya
			for (int j = arr.length-1; j > i; j--) {
				//if statement
				if (((arr[i]+arr[j])%k) ==0) {
					hasil++;
				}
			}
		}
		//return method
		return hasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int k = 3;
		int[] arr = {1,3,2,6,1,2};
		
		//manggil method dan nampilin output
		System.out.println(divisibleSumPairs(k, arr));
	}
}
