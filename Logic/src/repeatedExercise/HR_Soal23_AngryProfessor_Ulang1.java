package repeatedExercise;

public class HR_Soal23_AngryProfessor_Ulang1 {
	static String angryProfessor(int k, int[] a) {
		int count=0;
		for (int i = 0; i < a.length; i++) {
			if(a[i]<=0) {
				count++;
			}
		}
		if(count<k) {
			return "YES";
		} else {
			return "NO";
		}
	}
	public static void main(String[] args) {
		int[] a=new int[] {-1,-3,4,2};
		System.out.println(angryProfessor(3, a));
		System.out.println(angryProfessor(4, a));
	}
}
