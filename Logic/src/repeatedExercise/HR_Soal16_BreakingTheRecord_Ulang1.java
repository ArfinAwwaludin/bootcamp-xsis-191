package repeatedExercise;

public class HR_Soal16_BreakingTheRecord_Ulang1 {
	static int[] breakingRecords(int[] scores) {
        int h=scores[0]; int l=scores[0]; //h=highest score l=lowest score
        int[] record = new int[]{0,0}; //mencatat breaking record [0]->h [1]->l
        for(int i=0; i<scores.length; i++){
            if(scores[i]>h){ //jika score>h sebelumnya maka record[0]+1
                h=scores[i];
                record[0]++;
            }
            if(scores[i]<l){ //jika score<l sebelumnya maka record[1]+1
                l=scores[i];
                record[1]++;
            }
        }
        return record;
    }

    public static void main(String[] args){
        int[] score=new int[] {10,5,20,20,4,5,2,25,1}; //score
        int[] hasil=breakingRecords(score); //memanggil method breakingRecords
        System.out.println("Breaking Records");
        System.out.println("Best  : "+hasil[0]);
        System.out.println("Worst : "+hasil[1]);
    }

}
