package repeatedExercise;

import java.util.Scanner;

public class HR_Soal4_StairCase_Ulang1 {

	//membuat method
	public static void stairCase(int n) {
		
		//looping utk membuat staircase
		
		for (int i = 0; i < n; i++) {
			if (i==n) {
				for (int j = 0; j <= n; j++) {
					if (i+j >= n ) {
						System.out.print("#");
					} else {
						System.out.print("");
					}
				}
			} else {
				for (int j = 0; j <= n; j++) {
					if (i+j > n) {
						System.out.print("#");
					} else {
						System.out.print(" ");
					}
				}
			System.out.println();
			}
		}
		
	}
	
	public static void main(String[] args) {
		//instansiasi scanner
		Scanner scn = new Scanner(System.in);
		
		//deklarasi inputan
		int n = scn.nextInt();
		
		//memanggil method utk nampilin di console
		stairCase(n);
	}

}
