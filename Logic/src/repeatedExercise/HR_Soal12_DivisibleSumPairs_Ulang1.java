package repeatedExercise;

public class HR_Soal12_DivisibleSumPairs_Ulang1 {

	//membuat method
	public static int divisibleSumPair(int k, int[] arr) {
		//deklarasi var utk nampung peluang yg memenuhi
		int hasil = 0;
		
		//looping utk array arr
		for (int i = 0; i < arr.length; i++) {
			
			for (int j = arr.length-1; j > i; j--) {
				
				//if else statement utk kondisi2 dan aksi
				if ((arr[i]+arr[j])%k == 0) {
					hasil++;
				}
			}	
		}
		
		//return method
		return hasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int k = 3;
		int[] arr = {1,3,2,6,1,2};
		
		//manggil method dan nampilin hasil
		System.out.println(divisibleSumPair(k, arr));
	}
}
