package repeatedExercise;

public class HR_Soal5_MiniMaxSum_Ulang2 {
	
	//membuat method
	public static void miniMaxSum(int[] arr) {
		//deklarasi variabel
		int total = 0;
		int max = arr[0];
		int min = arr[0];
		
		//looping utk array
		for (int i = 0; i < arr.length; i++) {
			//perhitungan utk dapetin total nilai dari seluruh array
			total = total + arr[i];
			
			//if else statement utk ngecek indeks array ke-i itu 
			//lebih gede atau lebih kecil dr nilai maximum/minimum
			if (arr[i]>max) {
				max = arr[i];
			} else {
				min = arr[i];
			}
		}
		
		//nampilin hasil si method
		System.out.println("minimum : "+(total-max) + " "+ "maksimum : " +(total-min));
		
	}

	public static void main(String[] args) {
		//membuat inputan
		int arr[] = {1,2,3,4,5};
		
		//memanggil method dan menampilkan hasil
		miniMaxSum(arr);

	}

}
