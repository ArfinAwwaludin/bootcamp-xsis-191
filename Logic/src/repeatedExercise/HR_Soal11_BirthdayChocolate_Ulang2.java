package repeatedExercise;

public class HR_Soal11_BirthdayChocolate_Ulang2 {
	
	//membuat method
	public static int birthdayChocolate(int m, int d, int[] arr) {
		//inisialisasi variabel jumlah cacah kemungkinan
		int hasil = 0;
		
		//looping utk array]
		for (int i = 0; i < arr.length-m; i++) {
			//deklarasi total
			int total = 0;
			
			//looping for sebanyak m
			for (int j = 0; j < m; j++) {
				total = total + arr[i+j];
			}
			
			//if statement kalo total sejumlah d mana akan mengcount cacah hasi;
			if (total == d) {
				hasil++;
			}
		}
		//return method
		return hasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int d = 2;
		int m = 2;
		int[] arr = {1,1,1,1,1,1};
		
		//memanggil method dan menampilkan hasil
		System.out.println(birthdayChocolate(m, d, arr));
	}

}
