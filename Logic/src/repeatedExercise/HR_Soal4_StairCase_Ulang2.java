package repeatedExercise;

import java.util.Scanner;

public class HR_Soal4_StairCase_Ulang2 {
	
	//membuat method
	public static void stairCase(int n) {
		
		//program looping
		for (int i = 0; i < n; i++) {
			if (i==n) {
				for (int j = 0; j <= n; j++) {
					if (i+j>n) {
						System.out.print("#");
					} else {
						System.out.print("");
					}
				}
			} else {
				for (int k = 0; k <= n; k++) {
					if (i+k>n) {
						System.out.print("#");
					} else {
						System.out.print(" ");
					}
				}
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		//instansiasi inputan
		Scanner scn = new Scanner(System.in);
		
		//deklarasi variabel
		int n = scn.nextInt();
		
		//memanggil method dan menampilkan hasil
		stairCase(n);

	}

}
