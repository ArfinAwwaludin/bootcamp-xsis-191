package repeatedExercise;

public class HR_DiagonalDifference_Ulang1 {

	//membuat method
	public static int diagonalDifference(int [][] arr) {
		
		//deklarasi variabel utk menampung hasil penjumlahan diagonal 1 dengan nilai awal = 0
		int a = 0;
		//deklarasi variabel utk menampung hasil penjumlahan diagonal 2 dengan nilai awal = 0
		int b = 0;
		
		//memulai program looping utk array inputan
		for (int i = 0; i < arr.length; i++) {
			a += arr[i][i];
			b += arr[i][arr.length-1-i];
		}
		
		//return method
		if (b>a) {
			return b-a;
		} else {
			return a-b;
		}
		
	}
	
	public static void main(String[] args) {
		//membuat inputan
		int[][] arr = {{19,2,3},{4,1,6},{9,8,9}};
		
		//memanggil method dan menampilkan hasil
		System.out.println(diagonalDifference(arr));
		

	}

}
