package repeatedExercise;

public class HR_Soal14_TheHurdleRace_Ulang1 {

	static int hurdleRace(int k, int[] height) {
        int max=0; //tinggi maksimum awal
        for(int i=0;i<height.length;i++){
            if(height[i]>max){ //update tinggi maksimum
                max=height[i];
            }
        }
        int potion=0; //kebutuhan potion awal
        if(max>k){
            potion=max-k; //butuh potion sebesar max-k
        }
        return potion;

    }

    public static void main(String[] args){
    	int k=4; //kemampuan loncat natural
    	int[] height=new int[] {1,6,3,5,2}; //membuat array tinggi rintangan
    	int hasil=hurdleRace(k, height); //memanggil method hurdleRace
    	System.out.print("Potion : "+hasil);
    }
	
}
