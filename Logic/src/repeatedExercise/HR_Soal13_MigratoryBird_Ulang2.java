package repeatedExercise;

public class HR_Soal13_MigratoryBird_Ulang2 {
	
	//membuat method
	public static int migratoryBird(int[] arr) {
		
		//deklarasi array type utk nampung tipe2 burung yg ada
		int[] type = new int [6];
		
		//looping utk array
		for (int i = 0; i < type.length; i++) {
			
			//ngebandingin dgn index array yg lain 
			for (int j = 0; j < type.length; j++) {
				
				//if statement -> kalo arr[i] misal arr[0] = 1, sama dgn j, dimana j = 1, maka 
				//cacah index array pada type ditambah 1
				if (arr[i]==j) {
					type[j]++;
				}
			}
		}
		
		//deklarasi var max utk nampung 
		int max = type[0];
		int hasil = 0;
		
		for (int k = 0; k < type.length; k++) {
			if (type[k]>max) {
				max = type[k];
				hasil = k;
			}
		}
		
		//return method
		return hasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int[] arr = {1,4,4,4,5,3};
		
		//manggil method dan nampilin hasil
		System.out.println(migratoryBird(arr));
	}

}
