package repeatedExercise;

public class HR_Soal7_AppleAndOrange_Ulang1 {
	
	//bikin method
	public static void appleAndOrange(int s, int t, int a, int b, int[] apple, int[] orange) {
		
		//deklarasi variabel dan nilai awal
		int jmlApple = 0;
		int jmlOrange = 0;
		
		//looping utk array apple[]
		for (int i = 0; i < apple.length; i++) {
			//kondisi utk posisi apple
			if (a+apple[i] >= s && a+apple[i] <=t) {
				jmlApple++;
			}
		}
		
		//looping utk array orang[]
		for (int i = 0; i < orange.length; i++) {
			//kondisi utk posisi orange
			if (b+orange[i] >= s && b+orange[i] <= t) {
				//jika di posisi yg benar, maka jumlah orange akan bertambah 1
				jmlOrange++;
			}
		}
		
		//menampilkan hasil dr method
		System.out.println(jmlApple+ " " +jmlOrange);
	}

	public static void main(String[] args) {
		//inputan
		int s = 7;
		int t = 11;
		int a = 5;
		int b = 15;
		int [] apple = {-2,2,1};
		int [] orange = {5,-6};
		
		//memanggil method dan menampilkan hasil
		appleAndOrange(s, t, a, b, apple, orange);

	}

}
