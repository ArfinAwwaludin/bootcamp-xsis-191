package repeatedExercise;

/*
Calculate and print the sum of the elements in an array, 
keeping in mind that some of those integers may be quite large.

	Function Description
Complete the aVeryBigSum function in the editor below. 
It must return the sum of all array elements.

	aVeryBigSum has the following parameter(s):
ar: an array of integers .

	Input Format
The first line of the input consists of an integer
The next line contains space-separated integers contained in the array.

	Output Format
Print the integer sum of the elements in the array.

 */

public class HR_Soal3_AVeryBigSum_Ulang1 {
	//membuat method utk perhitungan
	public static long aVeryBigSum(long [] arr) {
		
		//deklarasi variabel dan nilai awal
		long jumlah = 0;
		
		//looping & perhitungan
		for(int i=0; i<arr.length; i++) {
			jumlah = jumlah+arr[i];
		}
		
		return jumlah;
	}
	
	//main method -> utk ngasih inputan, manggil method yg udah ngitung dan nampilin hasilnya
	public static void main(String[] args) {
		//inputan
		long [] arr = {100000000, 199999999, 2898988, 299999998};
		
		//manggil method dan nampung hasilnya ke suatu variabel
		long hasil = aVeryBigSum(arr);;
		System.out.println(hasil);
		
		
	}
	
}
