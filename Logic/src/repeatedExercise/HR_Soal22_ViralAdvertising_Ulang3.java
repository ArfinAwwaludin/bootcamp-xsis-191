package repeatedExercise;

public class HR_Soal22_ViralAdvertising_Ulang3 {

	//bikin method
	public static int viralAdvertising(int n) {
		//deklarasi nilai awal
		int share = 5;
		int like = 0;
		int cumulative = 0;
		
		//looping utk dapetin jumlah like di hari ke n
		for (int i = 0; i < n; i++) {
			like = share/2;
			cumulative = cumulative+like;
			share = like*3;
		}
		
		//return method
		return cumulative;
	}
	
	//main method
	public static void main(String[] args) {
		System.out.println(viralAdvertising(5));
	}
	
}
