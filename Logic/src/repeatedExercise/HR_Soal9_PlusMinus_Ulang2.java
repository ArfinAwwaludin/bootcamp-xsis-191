package repeatedExercise;

public class HR_Soal9_PlusMinus_Ulang2 {
	
	//membuat method
	public static void plusMinus(double[] arr) {
		//deklarasi variabel penampung jumlah cacah masing2 bilangan
		double nPositive = 0;
		double nNegative = 0;
		double nZero = 0;
		
		//looping utk array
		for (int i = 0; i < arr.length; i++) {
			//if else statement utk membuat tiap2 kondisi dan aksi yg akan dilakukan
			if (arr[i]>0) {
				nPositive++;
			} else if(arr[i]<0){
				nNegative++;
			}else {
				nZero++;
			}
		}
		
		//membuat output method
		System.out.println(nPositive/arr.length);
		System.out.println(nNegative/arr.length);
		System.out.println(nZero/arr.length);
		
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		double [] arr = {1,1,-1,0,0,-2,3,-1};
		
		//memanggil method utk nampilin output
		plusMinus(arr);
	}

}
