package repeatedExercise;

public class HR_DiagonalDifference_Ulang3 {
	
	//membuat method
	public static int diagonalDifference(int [][] arr) {
		//inisialisasi variabel utk menampung jumlah nilai variabel dgn nilai awal = 0
		int a = 0;
		int b = 0;
		
		//program looping untuk array
		for (int i = 0; i < arr.length; i++) {
			
			//"kalo indeks baris = indeks kolom, maka jumlahkan value pada indeks tersebut kemudian update 
			// pada variabel penampung yakni a"
			a += arr[i][i];
			
			//"kalo indeks baris = indeks kolom : panjang array-1-looping ke-i , 
			// maka jumlahkan value pada indeks tersebut kemudian update 
			// pada variabel penampung yakni b"
			b += arr[i][arr.length-1-i];
		}
		
		//return method
		if (a>b) {
			return a-b;
		}else {
			return b-a;
		}
		
	}

	public static void main(String[] args) {
		//inputan 
		int[][] arr = {{1,2,3},{4,5,6},{8,9,8}};
		
		//memanggil method dan menampilkan hasil
		System.out.println(diagonalDifference(arr));

	}

}
