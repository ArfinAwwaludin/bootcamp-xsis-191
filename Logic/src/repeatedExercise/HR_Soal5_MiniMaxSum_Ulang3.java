package repeatedExercise;

public class HR_Soal5_MiniMaxSum_Ulang3 {
	
	//membuat method
	public static void miniMaxSum(int[] arr) {
		//deklarasi variabel
		int total=0;
		int min = arr[0];
		int max = arr[0];
		
		//looping
		for (int i = 0; i < arr.length; i++) {
			//perhitungan total nilai array
			total = total+arr[i];
			
			//if else statement utk ngecek nilai pada index array ke-i
			if (arr[i]>max) {
				max=arr[i];
			} else {
				min=arr[i];
			}
		}
		
		//nampilin hasil dr method ini
		System.out.println((total-max)+ " " + (total-min));
		
	}
	
	public static void main(String[] args) {
		//membuat inputan
		int[] arr = {1,2,3,4,5};
		
		//manggil method dan nampilin hasil
		miniMaxSum(arr);

	}

}
