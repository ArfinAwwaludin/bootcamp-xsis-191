package repeatedExercise;

import java.util.*;

public class HR_Soal10_CompareTheTriplets_Ulang3 {

	//membuat method
	public static List<Integer> compareTheTriplets(List<Integer> a, List<Integer> b){
		
		//deklarasi variabel utk nampung kumulasi dari jumlah cacah
		int nAlice = 0;
		int nBob = 0;
		
		//looping for utk array
		for (int i = 0; i < a.size(); i++) {
			if (a.get(i)>b.get(i)) {
				nAlice++;
			} else if(a.get(i)<b.get(i)){
				nBob++;
			}
		}
		
		//deklarasi return
		List<Integer> hasil = new ArrayList<>();
		hasil.add(nAlice);
		hasil.add(nBob);
		
		//return method
		return hasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		List<Integer> a = new ArrayList<>();
		a.add(5);
		a.add(1);
		a.add(5);
		
		List<Integer> b = new ArrayList<>();
		b.add(2);
		b.add(3);
		b.add(4);
		
		//memanggil method dan menampilkan output
		System.out.println(compareTheTriplets(a, b));
		
		
	}
	
}
