package repeatedExercise;

public class HR_Soal18_CatAndAMouse_Ulang1 {
	static String catAndMouse(int x, int y, int z) {
		int difx=Math.abs(z-x);
		int dify=Math.abs(z-y);
		if(difx<dify) {
			return "Cat A";
		} else if(difx>dify) {
			return "Cat B";
		} else {
			return "Mouse C";
		}
	}
	public static void main(String[] args) {
		System.out.println(catAndMouse(1, 5, 3));
		System.out.println(catAndMouse(1, 5, 2));
		System.out.println(catAndMouse(1, 5, 4));
	}
}
