package repeatedExercise;

public class HR_Soal8_Kangaroo_Ulang2 {
	
	//bikin method
	public static String kangaroo(int x1, int v1, int x2, int v2) {
		
		//inisialisasi
		String a = "YES";
		
		//if else statement
		if (x1<x2 && v1<v2) {
			a = "NO";
		} else {
			if ((v1 != v2) && (x2-x1)%(v2-v1)==0) {
				a = "YES";
			} else {
				a = "NO";
			}
		}
		
		//return method
		return a;
		
	}

	public static void main(String[] args) {
		//inputan
		int x1 = 0;
		int v1 = 2;
		int x2 = 5;
		int v2 = 3;
		
		//menggil method dan output
		System.out.println(kangaroo(x1, v1, x2, v2));

	}

}
