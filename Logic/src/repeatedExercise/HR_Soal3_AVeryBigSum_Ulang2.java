package repeatedExercise;

public class HR_Soal3_AVeryBigSum_Ulang2 {

	//membuat method
	public static long aVeryBigSum(long[] arr) {
		
		//inisialisasi variabel jumlah utk menampung total jumlah nilai dr array
		long jumlah = 0;
		
		for (int i = 0; i < arr.length; i++) {
			//melakukan perhitungan dengan menambahkan jumlah sebelumnya dengan value array pada index ke-i
			jumlah = jumlah+arr[i];
		}
		
		//return method berupa nilai dr jumlah value dari seluruh array
		return jumlah;
		
	}
	
	public static void main(String[] args) {
		//membuat inputan
		long[] arr = {199999,998877,166677, 77862765, 77687};
		
		//memanggil method dan menampilkan hasil
		System.out.println("jumlah : " +aVeryBigSum(arr));

	}

}
