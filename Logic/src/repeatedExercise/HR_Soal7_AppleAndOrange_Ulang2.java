package repeatedExercise;

public class HR_Soal7_AppleAndOrange_Ulang2 {

	//membuat method
	public static void appleAndOrang(int s, int t, int a, int b, int[]apple, int[]orange) {
		
		//deklarasi variabel utk jumlah apple dan orange dgn nilain awal masing2 adlh 0
		int jmlApple = 0;
		int jmlOrange = 0;
		
		//looping utk array apple[]
		for (int i = 0; i < apple.length; i++) {
			if (a+apple[i]>=s && a+apple[i]<=t) {
				jmlApple++;
			}
		}
		
		//looping utk array orange[]
		for (int i = 0; i < orange.length; i++) {
			if (b+orange[i] >= s && b+orange[i] <= t) {
				jmlOrange++;
			}
		}
		
		//menampilkan hasil dr method ini
		System.out.println(jmlApple + " " + jmlOrange);
		
	}
	
	public static void main(String[] args) {
		//inputan
		int s = 7;
		int t = 10;
		int a = 4;
		int b = 12;
		int[] apple = {2,3,-4};
		int[] orange = {3,-2,-4};
		
		//memanggil method dan menampilkan hasilnya
		appleAndOrang(s, t, a, b, apple, orange);
	}

}
