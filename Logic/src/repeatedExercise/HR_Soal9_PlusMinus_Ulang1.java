package repeatedExercise;

public class HR_Soal9_PlusMinus_Ulang1 {
	
	//membuat method
	public static void plusMinus(double[] arr) {
		
		//inisialisasi variabel jumlah positif, negatif dan nol dgn nilai awal = 0 
		double nPositive = 0;
		double nNegative = 0;
		double nZero = 0;
		
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] < 0) {
				nNegative++;
			} else if(arr[i]>0){
				nPositive++;
			}else {
				nZero++;
			}
		}
		
		//menampilkan hasil dr method
		System.out.println(nPositive/arr.length);
		System.out.println(nNegative/arr.length);
		System.out.println(nZero/arr.length);
		
	}

	public static void main(String[] args) {
		//inputan
		double [] arr = {1, -1,-2,0,0,1,3};
		
		//manggil method dan nampilin hasil
		plusMinus(arr);
 
	}

}
