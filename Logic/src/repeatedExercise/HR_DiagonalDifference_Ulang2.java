package repeatedExercise;

public class HR_DiagonalDifference_Ulang2 {

	//membuat method
	public static int diagonalDifference(int[][] arr) {
		//inisialisasi variabel utk menampung nilai jumlah diagonal 1 dgn nilai awal = 0
		int a = 0;
		//inisialisasi variabel utk menampung nilai jumlah diagonal 2 dgn nilai awal = 0
		int b = 0;
		
		//program looping utk array
		for (int i = 0; i < arr.length; i++) {
			a += arr[i][i];
			b += arr[i][arr.length-1-i];
		}
		
		//return method
		if (a>b) {
			return a-b;
		} else {
			return b-a;
		}
		
	}
	
	public static void main(String[] args) {
		//inputan
		int[][] arr = {{1,2,3},{4,5,6},{9,8,8}};
		
		//memanggil method dan menampilkan hasil
		System.out.println(diagonalDifference(arr));
		

	}

}
