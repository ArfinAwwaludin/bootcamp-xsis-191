package repeatedExercise;

public class HR_Soal17_CamelCase_Ulang2 {
	//bikin method
	public static int camelCase(String s) {
		
		//membuat objek s1 dgn class String
		String[] s1 = s.split("(?=[A-Z])");

		int kata = s1.length;

		return kata;
	}
	
	//main method
	public static void main(String[] args) {
		System.out.println(camelCase("HiyaHiya"));
	}
}
