package repeatedExercise;

public class HR_Soal15_BonAppetit_Ulang1 {

	static void bonAppetit(int[] bill, int k, int b) {
		int anna=0;
		for (int i = 0; i < bill.length; i++) {
			if(k==i) {
				anna+=0;
			} else {
				anna+=bill[i];
			}
		}
		anna=anna/2;
		if(anna==b) {
			System.out.print("Bon Appetit");
		} else {
			int sisa=b-anna;
			System.out.print(sisa);
		}
	}
	public static void main(String[] args) {
		int[] bill=new int[] {1,2,3,4,5};
		bonAppetit(bill, 4, 5);
	}
}
