package repeatedExercise;

public class HR_Soal13_MigratoryBird {
	
	//bikin method
	public static int migratoryBird(int[] arr) {
		
		int[] type = new int[6];
		
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < type.length; j++) {
				if(arr[i]==j) {
					type[j]++;
				}
			}
		}
		
		int max=type[0];
		int hasil=0;
		
		for (int k = 0; k < type.length; k++) {
			if(type[k]>max) {
				max=type[k];
				hasil=k;
			}
		}
		//return method
		return hasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int[] arr = {1,4,4,5,5,3};
		
		//manggil method dan nampilin hasil
		System.out.println(migratoryBird(arr));
	}

}
