package repeatedExercise;

import java.util.Arrays;

public class HR_Soal6_GradingStudent_Ulang1 {

	//membuat method
	public static int[] gradingStudent(int[] arr) {
		
		//program looping
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]>=38) {
				if ((5-arr[i]%5)<3) {
					arr[i]=arr[i]+(5-arr[i]%5);
				}
			}
		}
		return arr;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int[] arr = {73,67,38,33};
		
		//manggil method dan nampilin hasil
		System.out.println(Arrays.toString(arr));
		
	}
	
}
