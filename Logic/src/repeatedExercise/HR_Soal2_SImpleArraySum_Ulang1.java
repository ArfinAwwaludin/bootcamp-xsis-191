package repeatedExercise;


/*
Given an array of integers, find the sum of its elements.
	
	Function Description
Complete the simpleArraySum function in the editor below. 
It must return the sum of the array elements as an integer.

	simpleArraySum has the following parameter(s):
ar: an array of integers
	
	Input Format
The first line contains an integer, n, denoting the size of the array.
The second line contains n space-separated integers representing the array's elements. 
 */

public class HR_Soal2_SImpleArraySum_Ulang1 {
	//membuat method ngitung jumlah vallue array
	public static int simpleArraySum(int[] arr) {
		//insisialisasi nilai jumlah awal
		int jumlah = 0;
		
		//looping 
		for (int i = 0; i < arr.length; i++) {
			jumlah = jumlah + arr[i];
		}
		
		return jumlah;
	}
	
	//membuat main class utk manggil method
	public static void main(String[] args) {
		
		//inputan
		int[] arr = {8,9,9,8,7,8,9};
		
		//manggil method, misal kita tampung dlm suatu variabel
		int hasil = simpleArraySum(arr);
		System.out.println(hasil);
		
	}
}
