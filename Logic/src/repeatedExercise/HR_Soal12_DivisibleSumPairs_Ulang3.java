package repeatedExercise;

public class HR_Soal12_DivisibleSumPairs_Ulang3 {

	//membuat method
	public static int divisibleSumPairs(int k, int[] arr) {
		//deklarasi var utk nampung cacah kemungkinan
		int hasil = 0;
		
		//looping utk array
		for (int i = 0; i < arr.length; i++) {
			//looping array lagi uyk ngebandingin dgn aindex2 setelahnya
			for (int j = arr.length-1; j > i; j--) {
				//if statement utk kondisi yg memenuhi dan aksi yg akan dilakukan
				if ((arr[i]+arr[j])%k ==0) {
					hasil++;
				}
			}
		}
		//return method berupa hasil cacah
		return hasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int k =3;
		int[] arr = {1,3,2,6,1,2};
		
		//manggil method dan nampilin hasil
		System.out.println(divisibleSumPairs(k, arr));
	}
	
}
