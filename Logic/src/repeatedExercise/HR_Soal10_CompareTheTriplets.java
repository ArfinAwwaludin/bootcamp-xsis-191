package repeatedExercise;

import java.util.Arrays;

public class HR_Soal10_CompareTheTriplets {
	
	//membuat method
	public static int[] compareTheTriplets(int[] a, int[] b) {
		
		//deklarasi variabel penampung kumulasi cacah poin alice dan bob
		int nAlice = 0;
		int nBob = 0;
		
		//for utk array
		for (int i = 0; i < a.length; i++) {
			if (a[i] > b[i]) {
				nAlice++;
			} else if (b[i] > a[i]) {
				nBob++;
			}
		}
		
		//deklarasi return array yg menampung integer nalice dan nbob
		int[] hasil = new int[2];
		hasil[0] = nAlice;
		hasil[1] = nBob;
		
		//return method
		return hasil;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int[] a = {5,6,7};
		int[] b = {3,6,10};
		
		//memanggil method dan menampilkan output
		System.out.println(Arrays.toString(compareTheTriplets(a, b)));
	}
}
