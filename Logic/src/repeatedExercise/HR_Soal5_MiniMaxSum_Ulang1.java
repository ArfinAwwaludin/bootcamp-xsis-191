package repeatedExercise;

public class HR_Soal5_MiniMaxSum_Ulang1 {
	
	//membuat method
	public static void miniMaxSum(int[] arr) {
		//deklarasi variabel total
		int max = arr[0];
		int min = arr[0];
		int total = 0;
		
		//looping utk array
		for (int i = 0; i < arr.length; i++) {
			//bikin perhitungan utk dapetin nilai total semua value pada array
			total = total + arr[i];
			
			//ngecek value tiap index array apakah lebih kecil atau lebih besar dr min dan max
			if (arr[i] > max) {
				max = arr[i];
			}else {
				min = arr[i];
			}
		}
		
		//nampilin hasil kalo maximal maka total-min(nilai terkecil
		//kalo minimal maka total-max(nilai terkecil)
		System.out.println((total-max) + " " + (total-min));
		
	}

	//main method
	public static void main(String[] args) {
		//inputan
		int [] arr = {1,2,3,4,5};

		//manggil method dan nampilin hasil
		miniMaxSum(arr);
		
	}

}
