package repeatedExercise;

public class HR_Soal2_SimpleArraySum_Ulang2 {

	//membuat method
	public static int simpleArraySum(int[] arr) {
		
		//inisialisai variabel utk menampung jumlah array dgn nilai awal 0 dan nama variabel = jumlah 
		int jumlah = 0;
		
		for (int i = 0; i < arr.length; i++) {
			//operasi utk menambahkan jumlah sebelumnya dangan value index array ke-i
			jumlah = jumlah + arr[i];
		}
		
		//return method = jumlah, karena yg diinginkan keluara dr method ini adalah jumlah/total 
		//penjumlahan dr seluruh value index array
		return jumlah;
		
	}
	
	public static void main(String[] args) {
		//inputan
		int[] arr = {1,2,3,4,5,5,6,36,34,64,325,2};
		
		//memanggil method dan menampilkan  hasil
		System.out.println(simpleArraySum(arr));

	}

}
