package repeatedExercise;

import java.util.Scanner;

public class HR_Soal4_StairCase_Ulang3 {

	//membuat method
	public static void stairCase(int n) {
		//membuat looping
		for (int i = 0; i < n; i++) {
			if (i==n) {
				for (int j = 0; j < n; j++) {
					if (i+j >= n) {
						System.out.print("#");
					} else {
						System.out.print("");
					}
				}
			} else {
				for (int j = 0; j < n; j++) {
					if (i+j >= n) {
						System.out.print("#");
					} else {
						System.out.print(" ");
					}
				}
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		//instansiasi scanner
		Scanner scn = new Scanner(System.in);
		
		//deklarasi variabel inputan
		int n = scn.nextInt();
		
		//memanggil method dan menampilkan hasil
		stairCase(n);

	}

}
