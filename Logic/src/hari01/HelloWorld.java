package hari01;

public class HelloWorld {
	
	public static void main(String[] args) {
		System.out.println("Welcome to Java");
		
		makanFavorit();
		sampleObject();
		
	}
	
	public static void makanFavorit () {
		System.out.println("1. soto");
		System.out.println("2. sate padang");
		System.out.println("3. nasi rames");
		System.out.println("4. nasgor");
		System.out.println("5. nasi padang");
		System.out.print("\n");
	}
	
	public static void sampleObject() {
		Orang or01= new Orang();
		or01.nama="Via";
		or01.alamat="Magelang";
		or01.jk="Wanita";
		or01.tempatLahir = "Lampung";
		or01.umur = 22 ;
		or01.cetak();

		Orang or02= new Orang();
		or02.nama="Siti";
		or02.alamat="jogja";
		or02.jk="wanita";
		or02.tempatLahir = "Lampung";
		or02.umur = 22 ;
		or01.cetak();
		
		Orang or03= new Orang();
		or03.nama="jaenab";
		or03.alamat="jogja";
		or03.jk="wanita";
		or03.tempatLahir = "Lampung";
		or03.umur = 22 ;
		or01.cetak();
		
		Orang or04= new Orang();
		or04.nama="bambang";
		or04.alamat="jogja";
		or04.jk="wanita";
		or04.tempatLahir = "Lampung";
		or04.umur = 22 ;
		or01.cetak();
		
		Orang or05= new Orang();
		or05.nama="sabran";
		or05.alamat="jogja";
		or05.jk="wanita";
		or05.tempatLahir = "Lampung";
		or05.umur = 22 ;
		or01.cetak();
		

	}
	
}
