package hari01;

public class BankSoal7 {

	public static void main(String[] args) {
		soal1(1);
		soal2(2);
		soal3(1);
		soal4(1);
		soal5(1);
		soal6(1);
		soal7(2);
		soal8(3);
		soal9(4);
		soal10(3);
	}
	
	//soal nomor 1
	public static int soal1(int a) {
		for(int i=0; i<7; i++) {
			System.out.print(a+ " ");
			a=a+2;
		}
		System.out.println();
		return a;
	}
	
	//soal nomor 2
	public static int soal2(int b) {
		for(int i=0; i<7; i++) {
			System.out.print(b+ " ");
			b=b+2;
		}
		System.out.println();
		return b;
	}
	
	//soal nomor 3
	public static int soal3(int c) {
		for(int i=0; i<7; i++) {
			System.out.print(c+ " ");
			c=c+3;
		}
		System.out.println();
		return c;
	}
	
	//soal nomor 4
	public static int soal4(int d) {
		for(int i=0; i<7; i++) {
			System.out.print(d+ " ");
			d=d+3;
		}
		System.out.println();
		return d;
	}
	
	//soal nomor 5
	public static int soal5(int e) {
		for(int i=0; i<7; i++) {
			if(i==2) {
				System.out.print("* ");
			}else if(i==5){
				System.out.print("* ");
			}else{
				System.out.print(e+ " ");
				e=e+4;
			}
		}
		System.out.println();
		return e;
	}
	
	//soal nomor 6
	public static int soal6(int f) {
		for(int i=0; i<7; i++) {
			if(i==2) {
				System.out.print("* ");
				f=f+4;
			}else if(i==5){
				System.out.print("* ");
				f=f+4;
			}else{
				System.out.print(f+ " ");
				f=f+4;
			}
		}
		System.out.println();
		return f;
	}
	
	//soal nomor 7
	public static int soal7(int g) {
		for(int i=0; i<7; i++) {
			System.out.print(g+ " ");
			g=g*2;
		}
		System.out.println();
		return g;
	}
	
	//soal nomor 8
	public static int soal8(int h) {
		for(int i=0; i<7; i++) {
			System.out.print(h+ " ");
			h=h*3;
		}
		System.out.println();
		return h;
	}
	
	//soal nomor 9
	public static int soal9(int i) {
		for(int j=0; j<7; j++) {
			if(j==2) {
				System.out.print("* ");
			}else if(j==5){
				System.out.print("* ");
			}else{
				System.out.print(i+ " ");
				i=i*4;
			}
		}
		System.out.println();
		return i;
	}
	
	//soal nomor 10
	public static int soal10(int j) {
		for(int i=0; i<7; i++) {
			if(i==3) {
				System.out.print("XXX ");
				j=j*3;
			}else{
				System.out.print(j+ " ");
				j=j*3;
			}
		}
		System.out.println();
		return j;
	}
}
