package hari01;

public class BankSoal {
	
	public static void main(String[] args) {
		
		// nomor 1
		int a = 1;
		for(int n = 0; n<7; n++) {
			System.out.print(a+ " ");
			a = a+2;
		}
		
		System.out.println(" ");
		
		//nomor 2
		int b = 2;
		for(int i = 0; i<7; i++) {
			System.out.print(b+ " ");
			b = b+2;
		}

		System.out.println(" ");
		
		//nomor 3
		int c = 1;
		for(int j = 0; j<7; j++) {
			System.out.print(c+ " ");
			c = c+3;
		}
		
		System.out.println(" ");
		
		//nomor 4
		int z = 1;
		for(int v = 0; v<7; v++) {
			System.out.print(z+ " ");
			z = z+3;
		}
		
		System.out.println(" ");
		
		//nomor 5
		int d = 1;
		
		for (int n = 0; n<7; n++) {
			if (n==2) {
				System.out.print("* ");
				continue;
			}else if (n==5) {
				System.out.print("* ");
				continue;
			}
			
			System.out.print(d+ " ");
			d=d+4;
		}
		
		System.out.println(" ");
		
		//nomor 6
		int e = 1;
		
		for (int n = 0; n<7; n++) {
			if (n==2 || n==5) {
				e=e+4;
				System.out.print("* ");
			}
			else {
			System.out.print(e+ " ");
			e=e+4;
			}
		}
		
		System.out.println(" ");
		
		//nomor 7
		int f = 2;
		for(int n = 0; n<7; n++) {
			System.out.print(f+ " ");
			f = f*2;
		}
		
		System.out.println(" ");
		
		//nomor 8
		int g = 3;
		for(int n = 0; n<7; n++) {
			System.out.print(g+ " ");
			g = g*3;
		}
		
		System.out.println(" ");

		//nomor 9
		int h = 4;
		
		for (int n = 0; n<7; n++) {
			if (n==2) {
				System.out.print("* ");
				continue;
			}else if (n==5) {
				System.out.print("* ");
				continue;
			}
			
			System.out.print(h+ " ");
			h=h*4;
		}
		
		System.out.println(" ");

		//nomor 10
		int i = 3;
		
		for (int n = 0; n<7; n++) {
			
			if (n==3) {
				i=i*3;
				System.out.print("XXX ");
			}
			else {
			System.out.print(i+ " ");
			i=i*3;
			}
		}
	}
}