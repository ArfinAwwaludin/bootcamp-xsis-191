package hari07;

public class InterfaceMain {
	public static void main(String[] args) {
		Bike02 obj01 = new Honda02();
		Bike02 obj02 = new Yamaha02();
		
		obj01.run();
		obj02.run();
	}
}
