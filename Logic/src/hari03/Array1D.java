package hari03;

public class Array1D {

	public static void main(String[] args) {
		//Array adalah struktur data yg fixed []
		//Aray itu tipe deata reference -> tipe data yg ada keyword "new" (instansiasi)
		
		//deklarasi array, index-nya dari 0-9
		int[] array = new int[10];
		//mengisi value array
		array[0]=1;
		array[1]=2;
		array[2]=3;
		array[3]=4;
		array[4]=5;
		array[5]=6;
		array[6]=7;
		array[7]=8;
		array[8]=9;
		array[9]=10;
		
		System.out.println(array.length);
		
		for(int i=0; i<array.length; i++) {
			System.out.print(array[i]+ " ");
		}
		
		System.out.println();
		
		//cara lain deklarasi array
		int[] array2 = new int[] {1,2,3,4,5};
		
		for(int j=0; j<array2.length; j++) {
			System.out.print(array2[j]+ " ");
		}
		
		

	}

}
