package hari03;

import java.util.*;

public class ArraySample01 {
	
	static Scanner scn;

	public static void main(String[] args) {
		// soal : 3 1 9 3 15 5 => n=6
		
		scn = new Scanner(System.in);
		
		System.out.print("inputan : ");
		int input = scn.nextInt();
		
		//inisialisasi array
		int nilaiAwal = 3;
		int nilaiAwal2 = 1;
		
		int[] array = new int [input];
		
		for(int i=0; i<array.length; i++) {
			if(i%2==0) {
				array[i]= nilaiAwal;
				nilaiAwal = nilaiAwal+6;
			}else {
				array[i]= nilaiAwal2;
				nilaiAwal2 = nilaiAwal2+2;
			}
			System.out.print(array[i]+" ");
		}

	}

}
