package hari03;
import java.util.*;

public class ArraySample01_2 {

	static Scanner scn;
	
	public static void main(String[] args) {
		// 3 1 9 3 15 5
		scn = new Scanner(System.in);
		
		System.out.print("inputkan jumlah deret : ");
		int input = scn.nextInt();
		
		//deklarasi nilai awal
		int angka1 = 3;
		int angka2 = 1;
		
		//deklarasi array
		int[] array = new int[input];
		
		for(int i=0; i<array.length; i++) {
			if(i%2==0) {
				array[i] = angka1;
				angka1=angka1+6;
			}else {
				array[i] = angka2;
				angka2=angka2+2;
			}
			System.out.print(array[i]+" ");
		}

	}

}
