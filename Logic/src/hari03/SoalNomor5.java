package hari03;
import java.util.*;

public class SoalNomor5 {

	static Scanner scn;
	
	public static void main(String[] args) {
		
		//inputan
		scn = new Scanner(System.in);
		
		System.out.print("inputan : ");
		String inputan = scn.next();
		
		//inisialisasi array utk mecah kalimat jadi kata
		String[] kata = inputan.split(" ");
		
		//mulai looping utk dapetin tiap kata dan dapetin tiap hurufnya
		for(int i=0; i<kata.length; i++) {
			
			//inisialisasi array untuk mecah kata jadi huruf
			String[] huruf = kata[i].split("");
			
			//looping buat dapetin tiap huruf
			for(int j=0; j<huruf.length; j++) {
				if(j>0 && j<huruf.length-1) {
					System.out.print("*");
				} else {
					System.out.print(huruf[j]);
				}
			}
			System.out.println();	
		}
	}
}
