package hari02;
import java.util.*;

public class SoalNomor1 {

	static Scanner scn;
	
	public static void main(String[] args) {
		
		System.out.println("========== Soal 1 ==========");
		
		scn = new Scanner(System.in);
		
		System.out.print("inputkan jumlah baris : ");
		int jumlahBaris = scn.nextInt();
		System.out.print("inputkan faktor pengali : ");
		int faktorPengali = scn.nextInt();
		
		int total = 0;
		int sukuAwal = 1;
		
		System.out.print("jawaban baris : ");
		
		for(int i=0; i<jumlahBaris; i++) {
			System.out.print(sukuAwal+ " ");
			
			if(i<3) {
				total = total + sukuAwal;
			}
			
			sukuAwal = sukuAwal*faktorPengali;
		}
		
		System.out.println();
		
		System.out.print("hasil perhitungan : " +total);

	}

}
