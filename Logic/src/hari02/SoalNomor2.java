package hari02;
import java.util.*;

public class SoalNomor2 {

	static Scanner scn;
	
	public static void main(String[] args) {
		
		System.out.println("========== Soal 2 ==========");
		
		scn = new Scanner(System.in);
		
		System.out.print("inputkan jumlah baris : ");
		int jumlahBaris = scn.nextInt();
		System.out.print("inputkan faktor pengali : ");
		int faktorPengali = scn.nextInt();
		
		int total = 1;
		int sukuAwal = 1;
		
		System.out.print("jawaban baris : ");
		
		for(int i=0; i<jumlahBaris; i++) {
			
			if(i<3) {
				total = total*sukuAwal*(-1);
			}
			
			if(i==2 || i==5) {
				sukuAwal = sukuAwal*(-1);
				System.out.print(sukuAwal+ " ");
				sukuAwal = sukuAwal*faktorPengali*(-1);
			}else {
				System.out.print(sukuAwal+ " ");
				sukuAwal = sukuAwal*faktorPengali;
			}
		}
		
		System.out.println();
		
		System.out.print("hasil perhitungan : " +total);

	}

}
