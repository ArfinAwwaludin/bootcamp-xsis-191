package hari05;

public class Tumbuhan {
	
	//properti
	public String namaTumbuhan;
	public String jenisTumbuhan;
	
	//constructor
	public Tumbuhan(String namaTumbuhan, String jenisTumbuhan) {
		this.namaTumbuhan=namaTumbuhan;
		this.jenisTumbuhan=jenisTumbuhan;
	}
	
	//method
	public void printTumbuhan() {
		System.out.println("nama tumbuhan : "+namaTumbuhan);
		System.out.println("jenis tumbuhan : "+jenisTumbuhan);
	}

}
