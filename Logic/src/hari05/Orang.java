package hari05;

public class Orang {

	//property
	int id;
	String nama;
	String alamat;
	String jk;
	int umur;
	
	//constructor
	public Orang() {
	}
	
	public Orang(int id, String nama, String alamat, String jk, int umur) {
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
		this.jk = jk;
		this.umur = umur;
	}
	
	//constructor
	public Orang(int id, String nama, String alamat) {
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
	}
	
	public void showData() {
		System.out.println("id : "+ id);
		System.out.println("nama : "+ nama);
		System.out.println("alamat : "+alamat);
		System.out.println("jk : "+jk);
		System.out.println("umur : "+umur);
	}
	
	

}
