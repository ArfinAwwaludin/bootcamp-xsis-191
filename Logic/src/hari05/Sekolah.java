package hari05;

public class Sekolah {
	
	//properti
	public int id;
	public String namaSekolah;
	public String alamatSekolah;
	public int jumlahSiswa;
	
	//constructor
	public Sekolah() {
		
	}
	
	public Sekolah(int id, String namaSekolah, String alamatSekolah, int jumlahSiswa) {
		this.id=id;
		this.namaSekolah=namaSekolah;
		this.alamatSekolah=alamatSekolah;
		this.jumlahSiswa=jumlahSiswa;
	}
	
	//method
	public void printSekolah() {
		System.out.println("id : "+id);
		System.out.println("nama sekolah : "+namaSekolah);
		System.out.println("alamat sekolah : "+alamatSekolah);
		System.out.println("jumlah siswa : "+jumlahSiswa);
	}

}
