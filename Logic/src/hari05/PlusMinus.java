package hari05;
import java.util.*;


public class PlusMinus {
	// Complete the plusMinus function below.
    static void plusMinus(int[] arr) {
        float nPositive = 0;
        float nNegative = 0;
        float nZero = 0;

        for(int i=0; i<arr.length; i++){
            if(arr[i] < 0){
                nNegative++;
            }else if(arr[i] == 0){
                nZero++;
            }else{
                nPositive++;
            }
        }

        System.out.println(nPositive/arr.length);
        System.out.println(nNegative/arr.length);
        System.out.println(nZero/arr.length);


    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        plusMinus(arr);

        scanner.close();
    }
}
