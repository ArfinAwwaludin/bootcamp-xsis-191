package hari05;

public class Hewan {

	String nama;
	String spesies;
	
	public Hewan() {
		
	}
	
	public Hewan(String nama, String spesies) {
		this.nama=nama;
		this.spesies=spesies;
		
	}
	
	void printHewan() {
		System.out.println("nama : "+nama);
		System.out.println("spesies : "+spesies);
	}
			
}
