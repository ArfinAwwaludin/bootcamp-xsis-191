package hari05;

public class MobilMain {

	public static void main(String[] args) {
		
		System.out.println("========mobil pertama =====");
		Mobil mobil1 = new Mobil("Mazda 2 GT", 2017, 2234);
		mobil1.printMobil();
		
		System.out.println("========mobil kedua =====");
		Mobil mobil2 = new Mobil("Honda Jazz RS Black Top Edition", 2018, 1445);
		mobil2.printMobil();
		
		System.out.println("========mobil ketiga =====");
		Mobil mobil3 = new Mobil("Mitsubishi Expander", 2018, 2256);
		mobil3.printMobil();
		
		System.out.println("========mobil keempat =====");
		Mobil mobil4 = new Mobil("Mitsubishi Outlander Sport", 2019, 9965);
		mobil4.printMobil();

	}

}
