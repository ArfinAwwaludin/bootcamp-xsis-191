package hari05;

public class Mobil {
	
	//deklarasi properti
	String namaMobil;
	int tahunRilis;
	int platNomor;
	
	public Mobil(String namaMobil, int tahunRilis, int platNomor) {
		this.namaMobil = namaMobil;
		this.tahunRilis = tahunRilis;
		this.platNomor = platNomor;
	}
	
	void printMobil() {
		System.out.println("nama mobil : " +namaMobil);
		System.out.println("nama tahun rilis : " +tahunRilis);
		System.out.println("nama plat nomor : " +platNomor);
	}
	
}
