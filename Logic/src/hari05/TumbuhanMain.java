package hari05;

public class TumbuhanMain {

	public static void main(String[] args) {
		Tumbuhan tanaman1 = new Tumbuhan("Oryza Sativa", "monokotil");
		tanaman1.printTumbuhan();
		
		Tumbuhan tanaman2 = new Tumbuhan("Zea Mays", "dikotil");
		tanaman2.printTumbuhan();

	}

}
