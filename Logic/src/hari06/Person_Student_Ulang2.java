package hari06;

public class Person_Student_Ulang2 {
	//main method
	public static void main(String[] args) {
		//bikin objek dr parent class / instansiasi
		ParentClass_Person_Ulang2 person1 = new ParentClass_Person_Ulang2(1, "bambang", "klaten", "pria");
		
		//manggil method void di parent class
		person1.printParentClass_Person_Ulang2();
		
		System.out.println("=====================================");
		
		//bikin objek dr child class
		ChildClass_Student_Ulang2 siswa1 = new ChildClass_Student_Ulang2(2235, 11, "IPS");
		
		//manggil method di child class
		siswa1.printChildClass_Student_Ulang2();
	}
}
