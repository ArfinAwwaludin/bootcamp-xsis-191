package hari06;

public class ParentClass_Person {
	
	//list property
	public int id;
	public String name;
	public String address;
	public String gender;
	
	//constructor 1
	public ParentClass_Person(int id, String name, String address, String gender) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.gender=gender;
		
	}
	
	//method
	public void printPerson() {
		System.out.println("id : "+id);
		System.out.println("name : "+name);
		System.out.println("address : "+address);
		System.out.println("gender : "+gender);
	}
	
	//empty constructor
	public ParentClass_Person() {
		
	}
}

