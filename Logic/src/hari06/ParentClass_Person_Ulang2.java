package hari06;

public class ParentClass_Person_Ulang2 {
	//membuat properti
	public int id;
	public String nama;
	public String alamat;
	public String gender;
	
	//constructor 1 - empty constructor
	public ParentClass_Person_Ulang2() {
		
	}
	
	//constructor 2
	public ParentClass_Person_Ulang2 (int id, String nama, String alamat, String gender) {
		this.id=id;
		this.nama=nama;
		this.alamat=alamat;
		this.gender=gender;
	}
	
	//method
	public void printParentClass_Person_Ulang2() {
		System.out.println("id/identitas : "+id);
		System.out.println("nama : "+nama);
		System.out.println("alamat : "+alamat);
		System.out.println("gender : "+gender);
	}
}
