package hari06;

public class Dosen{

	//properti
	public int noDosen;
	public String mataKuliah;
	public String fakultas;
	
	//constructor
	public Dosen(int noDosen, String mataKuliah, String fakultas) {
		this.noDosen=noDosen;
		this.mataKuliah=mataKuliah;
		this.fakultas=fakultas;
	}
	
	//method
	public void printDosen() {
		System.out.println("nomor Dosen : "+noDosen);
		System.out.println("mata kuliah : "+mataKuliah);
		System.out.println("fakultas : "+fakultas);
	}

}
