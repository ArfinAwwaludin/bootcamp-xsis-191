package hari06;

public class ChildClass_Bike {

	//defining same method as in the parent class
	void run() {
		System.out.println("Bike is running safely");
	}
	
	public static void main(String[] args) {
		//creating object
		ChildClass_Bike obj = new ChildClass_Bike();
		//calling method
		obj.run();
		
	}
	
}
