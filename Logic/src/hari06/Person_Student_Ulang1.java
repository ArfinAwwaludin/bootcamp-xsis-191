package hari06;

//membuat objek dr class parent dan child didalam main method
//memanggil method dr class parent dan child didalam main method

public class Person_Student_Ulang1 {

	public static void main(String[] args) {
		
		//bikin objek dr parent class dengan nama objek = person1 // instansiasi
		ParentClass_Person_Ulang1 person1 = new ParentClass_Person_Ulang1(1, "budi", "karawang", "laki laki");
		
		System.out.println("==================================================");
		
		//panggil method dr	parent classperson utk nampilin data
		person1.printParentClass_Person_Ulang1();
		
		System.out.println("==================================================");
		
		//bikin objek dr child class dgn nama objek student1 //instansiasi
		ChildClass_Student_Ulang1 student1 = new ChildClass_Student_Ulang1(2234, 12, "IPA");
		
		System.out.println("==================================================");
		
		//manggil method di child class
		student1.printChildClass_Student_Ulang1();
	} 
	
}
