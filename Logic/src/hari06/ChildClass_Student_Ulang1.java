package hari06;

public class ChildClass_Student_Ulang1 extends ParentClass_Person_Ulang1 {
	
	//properti
	public int nomorSiswa;
	public int grade;
	public String major;
	
	//constructor
	public ChildClass_Student_Ulang1(int nomorSiswa, int grade, String major) {
		this.nomorSiswa=nomorSiswa;
		this.grade=grade;
		this.major=major;
	}
	
	//method
	public void printChildClass_Student_Ulang1() {
		System.out.println("nomor siswa : "+nomorSiswa);
		System.out.println("grade : "+grade);
		System.out.println("major : "+major);
	}
	
	
}
