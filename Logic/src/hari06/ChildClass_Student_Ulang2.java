package hari06;

public class ChildClass_Student_Ulang2 extends ParentClass_Person_Ulang2 {

	//membuat properti
	public int nomorSiswa;
	public int grade;
	public String major;
	
	//constructor 1
	public ChildClass_Student_Ulang2(int nomorSiswa, int grade, String major) {
		this.nomorSiswa=nomorSiswa;
		this.grade=grade;
		this.major=major;
	}
	
	//method
	public void printChildClass_Student_Ulang2() {
		System.out.println("nomor siswa : "+nomorSiswa);
		System.out.println("grade : "+grade);
		System.out.println("major : "+major);
	}
	
}
