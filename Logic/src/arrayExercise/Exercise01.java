package arrayExercise;
import java.util.*;

public class Exercise01 {
	
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		
		System.out.println("==========Case Study Logic 01==========\n");
		
		System.out.println("==========soal nomor 1==========");
		System.out.print("inputkan jumlah deret : ");
		int jumlahDeret = scn.nextInt();
		System.out.print("inputkan nilai awal : ");
		int nilaiAwal = scn.nextInt(); 
		
		// soal nomor 1
		int [] array = new int[jumlahDeret];
		
		System.out.print("jawaban deret : ");
		
		for(int i=0; i<array.length; i++) {
			array[i]=nilaiAwal;
			System.out.print(array[i]+ " ");
			nilaiAwal=nilaiAwal+2;
		}
		
		System.out.println("==========soal nomor 2==========");
		System.out.print("inputkan jumlah deret : ");
		int jumlahDeret2 = scn.nextInt();
		System.out.print("inputkan nilai awal : ");
		int nilaiAwal2 = scn.nextInt(); 
		
		int[] array2 = new int[jumlahDeret2];
		
		for(int i=0; i<array2.length; i++) {
			array2[i]=nilaiAwal2;
			System.out.print(array2[i]+" ");
			nilaiAwal2=nilaiAwal2+2;
		}
		
		System.out.println("==========soal nomor 5==========");
		System.out.print("inputkan jumlah deret : ");
		int jumlahDeret3 = scn.nextInt();
		System.out.print("inputkan nilai awal : ");
		int nilaiAwal3 = scn.nextInt();
		
		int [] array3 = new int [jumlahDeret3];
		
		for(int i=0; i<array3.length; i++) {
			array3[i]=nilaiAwal3;
			
			if(i==2 || i==5) {
				System.out.print("* ");
			}else {
				nilaiAwal3=nilaiAwal3+4;
				System.out.print(array3[i]+" ");
			}
		}
		
		
	}

}
