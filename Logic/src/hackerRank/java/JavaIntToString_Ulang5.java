package hackerRank.java;

import java.util.Scanner;

public class JavaIntToString_Ulang5 {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn5 = new Scanner(System.in);
			
		//var utk input
		int n5 = scn5.nextInt();
		
		//int to string
		String s5 = Integer.toString(n5);
		
		//if else statement
		if (n5 == Integer.parseInt(s5)) {
			System.out.println("good job");
		} else {
			System.out.println("wrong answer");
		}
	}
}
