package hackerRank.java;

import java.util.Scanner;

public class JavaIntToString {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn1 = new Scanner(System.in);
		
		//var utk inputan
		int n1 = scn1.nextInt();
		
		//int to string
		String s1 = Integer.toString(n1);
		
		//if else statement
		if (n1==Integer.parseInt(s1)) {
			System.out.println("good job");
		} else {
			System.out.println("wrong answer");
		}
	}
}
