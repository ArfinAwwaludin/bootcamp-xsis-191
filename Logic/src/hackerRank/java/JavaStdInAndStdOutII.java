package hackerRank.java;
//import java.util
import java.util.*;

public class JavaStdInAndStdOutII {
	//membuat fungsi main
	public static void main(String[] args) {
		//memanggil Scanner
		Scanner scn1= new Scanner(System.in);
		
		//membuat variabel utk inputan
		int a1 = scn1.nextInt();
		Double d1 = scn1.nextDouble();
		String s1  = scn1.next();
		
		//menampilkan inputan 
		System.out.println(s1);
		System.out.println(d1);
		System.out.println(a1);
		
	}

}
