package hackerRank.java;

import java.util.Scanner;

public class JavaIfElse_Ulang5 {
	//membuat main method
	public static void main(String[] args) {
		//memanggil scanner/instansiasi
		Scanner scn5 = new Scanner(System.in);
		
		//membuat variabel utk inputan
		int a5 = scn5.nextInt();
		
		//program
		if (a5%2==1) {
			System.out.println("weird");
		}else {
			if (a5>=2 && a5<=5) {
				System.out.println("not weird");
			}else if (a5>=6 && a5<=20) {
				System.out.println("weird");
			}else {
				System.out.println("not weird");
			}
		}
	}

}
