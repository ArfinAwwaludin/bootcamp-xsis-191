package hackerRank.java;

import java.util.Scanner;

public class JavaDataTypes_Ulang5 {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn5 = new Scanner(System.in);
		
		//var utk loop
		int n5 = scn5.nextInt();
		
		//looping
		for (int m = 0; m < n5; m++) {
			try {
				long x5 = scn5.nextLong();
				System.out.println("merupakan : ");
				if(x5>=-128 && x5<=127)
					System.out.println("* byte");
				if(x5>=(Math.pow(2,15)*-1) && x5<=Math.pow(2,15)-1) 
					System.out.println("* short");
				if(x5>=(Math.pow(2,31)*-1) && x5<=Math.pow(2,31)-1) 
					System.out.println("* int");
				if(x5>=(Math.pow(2,63)*-1) && x5<=Math.pow(2,63)-1)
					System.out.println("* long");
			} catch (Exception e) {
				System.out.println(scn5.next()+" can't be fitted anywhere.");
			}
		}
	}
}
