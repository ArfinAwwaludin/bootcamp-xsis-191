package hackerRank.java;

import java.util.Scanner;

public class JavaStdInAndStdOutI {
	
	//membuat fungsi main
	public static void main(String[] args) {
		//memanggil Scanner 
		Scanner scn1 = new Scanner(System.in);
			
		//membuat variabel untuk inputan
		int a1 = scn1.nextInt();
		int b1 = scn1.nextInt();
		int c1 = scn1.nextInt();
			
		//menampilkan inputan ke consol
		System.out.println(a1);
		System.out.println(b1);
		System.out.println(c1);
			
			
	}
}
