package hackerRank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock_Ulang2 {

	static int B2;
	static int H2;
	static boolean flag2=false;
	static {
		//instansiasi
		Scanner scn2 = new Scanner(System.in);
		B2 = scn2.nextInt();
		H2 = scn2.nextInt();
		
		//if statement
		if (B2>0 && H2>0) {
			flag2=true;
		} else {
			System.out.print( "java.lang.Exception: Breadth and height must be positive" );
		}
		
	}
	
	//main method
	public static void main(String[] args) {
		if (flag2) {
			int area2 = B2 * H2;
			System.out.println(area2);
		}
	}
	
}
