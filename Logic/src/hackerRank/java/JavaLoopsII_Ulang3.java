package hackerRank.java;

import java.util.Scanner;

public class JavaLoopsII_Ulang3 {
	//main method
	public static void main(String[] args) {
		
		//instansiasi scanner
		Scanner scn3 =  new Scanner(System.in);

		//variabel utk banyak looping
		int t3 = scn3.nextInt();
		//looping ke-1
		for(int k = 0; k < t3; k++){
			//variabel utk looping 2
			int a3 = scn3.nextInt();
			int b3 = scn3.nextInt();
			int n3 = scn3.nextInt();
		           
	       //looping ke-2
	       for(int l = 0; l < n3; l++){
	         	a3 = a3+b3;
	           System.out.print(a3+ " ");
	           b3 = 2*b3;
	        }
	        System.out.println();
	     }
	}
}
