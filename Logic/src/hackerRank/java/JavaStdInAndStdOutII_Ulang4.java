package hackerRank.java;

import java.util.Scanner;

public class JavaStdInAndStdOutII_Ulang4 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn4 = new Scanner(System.in);
		
		//variabel utk input
		int i4 = scn4.nextInt();
		Double d4 = scn4.nextDouble();
		String s4 = scn4.next();
		
		//menampilkan input
		System.out.println(s4);
		System.out.println(d4);
		System.out.println(i4);
	}
}
