package hackerRank.java;

import java.util.Scanner;

public class JavaStdInAndStdOutII_Ulang6 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn6 = new Scanner(System.in);
		//var utk input
		int i6 = scn6.nextInt();
		Double d6 = scn6.nextDouble();
		String s6 = scn6.next();
		
		//menampilkan input
		System.out.println(s6);
		System.out.println(d6);
		System.out.println(i6);
	}
}
