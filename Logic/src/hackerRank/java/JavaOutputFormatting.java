package hackerRank.java;
//import java.util
import java.util.*;

public class JavaOutputFormatting {
	//membuat fungsi main
	public static void main(String[] args) {
		//memanggil scanner
		Scanner scn1 = new Scanner(System.in);
		
		//program
		System.out.println("================================");
		for(int i=0; i<3; i++) {
			//variabel utk inputan
			String a1 = scn1.next();
			int b1 = scn1.nextInt();
			System.out.printf("%-15s%03d%n",a1,b1);
		}
		System.out.println("================================");
	}
}
