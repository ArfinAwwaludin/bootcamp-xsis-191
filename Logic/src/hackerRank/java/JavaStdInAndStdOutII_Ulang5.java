package hackerRank.java;

import java.util.Scanner;

public class JavaStdInAndStdOutII_Ulang5 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn5 = new Scanner(System.in);
		
		//var utk input
		int i5 = scn5.nextInt();
		Double d5 = scn5.nextDouble();
		String s5 = scn5.next();
		
		//menampilkan input
		System.out.println(s5);
		System.out.println(d5);
		System.out.println(i5);
	}
}
