package hackerRank.java;
//import java.util
import java.util.*;

public class JavaIfElse {
	
	//membuat fungsi main
	public static void main(String[] args) {
	
		//memanggil scanner
		Scanner scn1 = new Scanner(System.in);
		
		//variabel utk mengambil inputan
		int a1 = scn1.nextInt();
		
		//program
		if(a1%2==1 ) {
			System.out.println("weird");
		}else {
			if(a1>=2 && a1<=5) {
				System.out.println("not weird");
			}else if(a1>=6 && a1<=20) {
				System.out.println("weird");
			}else {
				System.out.println("not weird");
			}
		}
	}
}
