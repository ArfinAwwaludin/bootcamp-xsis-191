package hackerRank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock {
	
	static int B;
	static int H;
	static Boolean flag = false;
	static{
		Scanner scn1 = new Scanner(System.in);
		B = scn1.nextInt();
		H = scn1.nextInt();
		
		//if statement
		if(B > 0 && H > 0){
			flag = true;
			}else{
				System.out.print( "java.lang.Exception: Breadth and height must be positive" );
			}
		}

	//main method
	public static void main(String[] args){
		//if statement
		if(flag){
			int area = B * H;
			System.out.print(area);
		}
	}
}
