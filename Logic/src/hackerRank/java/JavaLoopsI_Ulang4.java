package hackerRank.java;

import java.util.Scanner;

public class JavaLoopsI_Ulang4 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn4 = new Scanner(System.in);
		
		//variabel utk inputan
		int a4 = scn4.nextInt();
		
		//program loop
		for (int l = 0; l < 10; l++) {
			int hasil4 = a4*l;
			System.out.println(a4+ " x "+l+" = " +hasil4);
		}
	}
}
