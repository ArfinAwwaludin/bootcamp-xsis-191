package hackerRank.java;

import java.util.Scanner;

public class JavaDataTypes_Ulang4 {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn4 = new Scanner(System.in);
		
		//var utk loop
		int n4 = scn4.nextInt();
		
		//looping
		for (int l = 0; l < args.length; l++) {
			try {
				long x4 = scn4.nextLong();
				System.out.println("merupakan : ");
				
				if(x4>=-128 && x4<=127)
					System.out.println("* byte");
				if(x4>=(Math.pow(2,15)*-1) && x4<=Math.pow(2,15)-1) 
					System.out.println("* short");
				if(x4>=(Math.pow(2,31)*-1) && x4<=Math.pow(2,31)-1) 
					System.out.println("* int");
				if(x4>=(Math.pow(2,63)*-1) && x4<=Math.pow(2,63)-1)
					System.out.println("* long");
			} catch (Exception e4) {
				System.out.println(scn4.next()+" can't be fitted anywhere.");
			}
		}
	}
}
