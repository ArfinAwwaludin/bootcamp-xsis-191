package hackerRank.java;

import java.util.Scanner;

public class JavaIntToString_Ulang3 {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn3 = new Scanner(System.in);
		
		//var utk input
		int n3 = scn3.nextInt();
		
		//int to string
		String s3 = Integer.toString(n3);
		
		//if else statement
		if (n3 == Integer.parseInt(s3)) {
			System.out.println("good job");
		} else {
			System.out.println("wrong answer");
		}
	}
}
