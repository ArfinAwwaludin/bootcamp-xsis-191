package hackerRank.java;

import java.util.Scanner;

public class JavaIntToString_Ulang2 {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn2 = new Scanner(System.in);
		
		//var utk inputan
		int n2 = scn2.nextInt();
		
		//int to string
		String s2 = Integer.toString(n2);
		
		//if statement
		if (n2 == Integer.parseInt(s2)) {
			System.out.println("good job");
		} else {
			System.out.println("wrong answer");
		}
	}
}
