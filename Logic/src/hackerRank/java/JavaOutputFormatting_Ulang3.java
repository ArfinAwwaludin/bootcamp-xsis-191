package hackerRank.java;

import java.util.Scanner;

public class JavaOutputFormatting_Ulang3 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn3 = new Scanner(System.in);
		
		//program
		System.out.println("======================");
		for (int k = 0; k < 3; k++) {
			//variabel utk inputan
			String a3 = scn3.next();
			int b3 = scn3.nextInt();
			System.out.printf("%-15s%03d%n",a3,b3);
		}
	}
}
