package hackerRank.java;

import java.util.Scanner;

public class JavaLoopsI_Ulang3 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn3 = new Scanner(System.in);
		
		//variabel utk inputan
		int a3 = scn3.nextInt();
		
		//program loop
		for (int k = 0; k < 10; k++) {
			int hasil3 = a3*k;
			System.out.println(a3+ " x " +k+ " = " +hasil3);
		}
	}
}
