package hackerRank.java;

import java.util.Scanner;

public class JavaLoopsI_Ulang5 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn5 = new Scanner(System.in);
		
		//variabel utk inputan
		int a5 = scn5.nextInt();
		
		//program looping
		for (int m = 0; m < 10; m++) {
			int hasil5 = a5*m;
			System.out.println(a5+" x "+m+" = " +hasil5);
		}
	}
}
