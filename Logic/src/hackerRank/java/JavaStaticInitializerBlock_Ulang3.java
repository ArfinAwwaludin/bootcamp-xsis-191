package hackerRank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock_Ulang3 {

	static int B3;
	static int H3;
	static boolean flag3 = false;
	static {
		//instansiasi
		Scanner scn3 = new Scanner(System.in);
		
		B3 = scn3.nextInt();
		H3 = scn3.nextInt();
		
		//if statement
		if (B3>0 && H3>0) {
			flag3 = true;
		} else {
			System.out.print( "java.lang.Exception: Breadth and height must be positive" );
		}
	}
	
	//main method
	public static void main(String[] args) {
		if (flag3) {
			int area3 = B3 * H3;
			System.out.println(area3);
		}
	}
}
