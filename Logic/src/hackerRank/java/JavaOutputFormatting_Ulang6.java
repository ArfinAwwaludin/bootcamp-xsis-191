package hackerRank.java;

import java.util.Scanner;

public class JavaOutputFormatting_Ulang6 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn6 = new Scanner(System.in);
		
		//program
		System.out.println("===================");
		for (int m = 0; m < 3; m++) {
			//variabel utk inputan
			String a6 = scn6.next();
			int b6 = scn6.nextInt();
			System.out.printf("%-15s%03d%n",a6,b6);
		}
		System.err.println("===================");
	}
}
