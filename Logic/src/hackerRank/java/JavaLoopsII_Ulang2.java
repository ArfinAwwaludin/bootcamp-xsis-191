package hackerRank.java;

import java.util.Scanner;

public class JavaLoopsII_Ulang2 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn2 =  new Scanner(System.in);
		
		//variabel utk banyak looping
		int t2 = scn2.nextInt();
		
		//looping ke-1
		for(int i = 0; i < t2; i++){
			//variabel utk looping 2
	       	int a2 = scn2.nextInt();
	        int b2 = scn2.nextInt();
	        int n2 = scn2.nextInt();
	           
	        //looping ke-2
	        for(int j = 0; j < n2; j++){
	          	a2 = a2+b2;
	            System.out.print(a2+ " ");
	            b2 = 2*b2;
	         }
	         System.out.println();
	     }
	}
}
