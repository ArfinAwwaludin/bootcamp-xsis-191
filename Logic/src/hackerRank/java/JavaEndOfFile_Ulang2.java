package hackerRank.java;

import java.util.Scanner;

public class JavaEndOfFile_Ulang2 {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn2 = new Scanner(System.in);
		
		//looping 
		for (int j = 0; scn2.hasNext(); j++) {
			String kata2 = scn2.nextLine();
			System.out.print(j+ " "+ kata2+ "\n");
		}
	}
}
