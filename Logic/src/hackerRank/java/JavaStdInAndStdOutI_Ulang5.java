package hackerRank.java;
//import java.util
import java.util.*;

public class JavaStdInAndStdOutI_Ulang5 {
	//membuat fungsi main
	public static void main(String[] args) {
		//memanggil scanner
		Scanner scn5 = new Scanner(System.in);
		
		//membuat variabel utk inputan
		int a5 = scn5.nextInt();
		int b5 = scn5.nextInt();
		int c5 = scn5.nextInt();
		
		//menampilkan inputan pada console
		System.out.println(a5);
		System.out.println(b5);
		System.out.println(c5);
	}
}
