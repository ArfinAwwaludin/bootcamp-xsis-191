package hackerRank.java;

import java.util.Scanner;

public class JavaLoopsI_Ulang6 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn6 = new Scanner(System.in);
		
		//variabel utk inputan
		int a6 = scn6.nextInt();
		
		//program looping
		for (int n = 0; n < 10; n++) {
			int hasil6 = a6*n;
			System.out.println(a6+" x "+n+ " = " +hasil6);
		}
	}

}
