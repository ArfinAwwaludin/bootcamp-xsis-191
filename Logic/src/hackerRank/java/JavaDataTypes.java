package hackerRank.java;

import java.util.Scanner;

public class JavaDataTypes {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn1 = new Scanner(System.in);
		
		//var utk banyak loop
		int n1 = scn1.nextInt();

		for (int i = 0; i < n1; i++) {
			try {
				//var utk inputan  nilai
				long x1 = scn1.nextLong();
				System.out.println(x1 + "merupakan : ");
			
				if(x1>=-128 && x1<=127)
					System.out.println("* byte");
				if(x1>=(Math.pow(2,15)*-1) && x1<=Math.pow(2,15)-1) 
					System.out.println("* short");
				if(x1>=(Math.pow(2,31)*-1) && x1<=Math.pow(2,31)-1) 
					System.out.println("* int");
				if(x1>=(Math.pow(2,63)*-1) && x1<=Math.pow(2,63)-1)
					System.out.println("* long");
            	}
				
				catch(Exception e1){
					System.out.println(scn1.next()+" can't be fitted anywhere.");
            	}
		}
	}
}
