package hackerRank.java;

import java.util.Scanner;

public class JavaEndOfFile {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn1 = new Scanner(System.in);
		
		//looping
		for (int i = 0; scn1.hasNext(); i++) {
			String kata1 = scn1.nextLine();
			System.out.print(i+ " "+ kata1+ "\n");
		}
	}
}
