package hackerRank.java;

import java.util.Scanner;

public class JavaIfElse_Ulang6 {
	
	//membuat main method
	public static void main(String[] args) {
	
		//memanggil Scanner/intansiasi
		Scanner scn6 = new Scanner(System.in);
		
		//membuat variabel utk inputan 
		int a6 = scn6.nextInt();
		
		//program
		if(a6%2==1) {
			System.out.println("weird");
		}else {
			if (a6>=2 && a6<=5) {
				System.out.println("not wierd");
			}else if (a6>=6 && a6<=20) {
				System.out.println("weird");
			}else {
				System.out.println("not weird");
			}
		}
	}
}
