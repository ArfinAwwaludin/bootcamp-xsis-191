package hackerRank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock_Ulang4 {

	static int B4;
	static int H4;
	static boolean flag4=false;
	static {
		//instansiasi
		Scanner scn4 = new Scanner(System.in);
		
		B4 = scn4.nextInt();
		H4 = scn4.nextInt();
		
		//if statement
		if (B4 > 0 && H4 > 0) {
			flag4=true;
		} else {
			System.out.print( "java.lang.Exception: Breadth and height must be positive" );
		}
	}
	
	//main method
	public static void main(String[] args) {
		int area4 = B4 * H4;
		System.out.println(area4);
	}
}
