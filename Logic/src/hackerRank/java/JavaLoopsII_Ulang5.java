package hackerRank.java;

import java.util.Scanner;

public class JavaLoopsII_Ulang5 {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn5 =  new Scanner(System.in);

		//variabel 
		int t5 = scn5.nextInt();

		//looping ke-1
		for(int k = 0; k < t5; k++){
			//variabel utk looping 2
			int a5 = scn5.nextInt();
			int b5 = scn5.nextInt();
			int n5 = scn5.nextInt();

			//looping ke-2
			for(int l = 0; l < n5; l++){
				a5 = a5+b5;
				System.out.print( a5+ " ");
				b5 = 2 * b5;
			}
			System.out.println();
		}
	}
}
