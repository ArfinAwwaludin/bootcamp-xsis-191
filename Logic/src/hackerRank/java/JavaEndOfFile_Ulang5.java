package hackerRank.java;

import java.util.Scanner;

public class JavaEndOfFile_Ulang5 {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn5 = new Scanner(System.in);
			
		//looping
		for (int m = 0; scn5.hasNext(); m++) {
			String kata5 = scn5.nextLine();
			System.out.print(m+ " "+ kata5+ "\n");
		}
	}
}
