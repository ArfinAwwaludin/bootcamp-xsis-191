package hackerRank.java;

import java.util.Scanner;

public class JavaOutputFormatting_Ulang2 {
	//membuat main method 
	public static void main(String[] args) {
		//instansiasi
		Scanner scn2 = new Scanner(System.in);
		
		//program
		System.out.println("======================");
		for (int j = 0; j < 3; j++) {
			//variabel utk inputan
			String a2 = scn2.next();
			int b2 = scn2.nextInt();
			System.out.printf("%-15s%03d%n",a2,b2);
		}
		System.out.println("======================");
	}
}
