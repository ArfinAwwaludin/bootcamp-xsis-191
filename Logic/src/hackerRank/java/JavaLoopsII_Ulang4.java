package hackerRank.java;

import java.util.Scanner;

public class JavaLoopsII_Ulang4 {
	
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn4 =  new Scanner(System.in);

		//variabel utk banyak looping
		int t4 = scn4.nextInt();

		//looping ke-1
		for(int k = 0; k < t4; k++){
			//variabel utk looping 2
			int a4 = scn4.nextInt();
			int b4 = scn4.nextInt();
			int n4 = scn4.nextInt();

			//looping ke-2
			for(int l = 0; l < n4; l++){
				a4 = a4+b4;
				
				System.out.print(a4+ " ");
				b4 = 2*b4;
			}
			System.out.println();
		}
	}
}