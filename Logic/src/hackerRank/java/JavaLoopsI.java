package hackerRank.java;
//import java.util
import java.util.*;

public class JavaLoopsI {
	//membuat fungsi main
	public static void main(String[] args) {
		//memanggil scanner
		Scanner scn1 = new Scanner(System.in);
		
		//membuat variabel untuk inputan
		int a1 = scn1.nextInt();
		
		//program loop
		for(int i=0; i<10; i++) {
			int hasil1 = a1*i;
			System.out.println(a1+ " x " +i+ " = " +hasil1);
		}
	}

}
