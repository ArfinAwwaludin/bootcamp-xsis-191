package hackerRank.java;
//import java.util
import java.util.*;

public class JavaStdInAndStdOutI_Ulang4 {
	
	//membuat fungsi main
	public static void main(String[] args) {
		//memanggil Scanner
		Scanner scn4 = new Scanner(System.in);
		
		//membuat variabel utk inputan
		int a4 = scn4.nextInt();
		int b4 = scn4.nextInt();
		int c4 = scn4.nextInt();
		
		//menampilkan inputan pada console
		System.out.println(a4);
		System.out.println(b4);
		System.out.println(c4);
	}
}
