package hackerRank.java;

import java.util.Scanner;

public class JavaEndOfFile_Ulang4 {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn4 = new Scanner(System.in);
		
		//looping
		for (int l = 0; scn4.hasNext(); l++) {
			String kata4 = scn4.nextLine();
			System.out.print(l+ " "+ kata4+ "\n");
		}
	}
}
