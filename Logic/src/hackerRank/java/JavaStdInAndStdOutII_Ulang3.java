package hackerRank.java;

import java.util.Scanner;

public class JavaStdInAndStdOutII_Ulang3 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn3 = new Scanner(System.in);
		
		//variabel utk input
		int i3 = scn3.nextInt();
		Double d3 = scn3.nextDouble();
		String s3 = scn3.next();
		
		//menampilkan inputan
		System.out.println(s3);
		System.out.println(d3);
		System.out.println(i3);
	}
}
