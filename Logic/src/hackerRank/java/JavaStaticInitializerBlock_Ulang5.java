package hackerRank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock_Ulang5 {
	static int B5;
	static int H5;
	static boolean flag5 = false;
	static {
		//instansiasi
		Scanner scn5 = new Scanner(System.in);
		
		B5 = scn5.nextInt();
		H5 = scn5.nextInt();
		
		//if statement
		if (B5>0 && H5>0) {
			flag5=true;
		} else {
			System.out.print( "java.lang.Exception: Breadth and height must be positive" );
		}
	}
	//main method
	public static void main(String[] args) {
		int area5 = B5 * H5;
		System.out.println(area5);
	}
}
