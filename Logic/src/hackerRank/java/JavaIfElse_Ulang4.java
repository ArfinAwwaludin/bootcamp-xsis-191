package hackerRank.java;

import java.util.Scanner;

public class JavaIfElse_Ulang4 {
	//membuat main method
	public static void main(String[] args) {
		//memanggil Scanner/intasiasi
		Scanner scn4 = new Scanner(System.in);
		
		//membuat variabel utk inputan
		int a4 = scn4.nextInt();
		
		//program
		if (a4%2==1) {
			System.out.println("weird");
		}else {
			if (a4>=2&&a4<=5) {
				System.out.println("not weird");
			}else {
				if (a4>=6 && a4<=20) {
					System.out.println("weird");
				}else {
					System.out.println("not weird");
				}
			}
		}
	}
}
