package hackerRank.java;

import java.util.Scanner;

public class JavaIfElse_Ulang3 {
	//membuat main method
	public static void main(String[] args) {
		//memanggil scanner
		Scanner scn3 = new Scanner(System.in);
		
		//membuat variabel utk inputan
		int a3 = scn3.nextInt();
		
		//program
		if(a3%2==1) {
			System.out.println("weird");
		}else {
			if(a3>=2 && a3<=5) {
				System.out.println("not weird");
			}else if (a3>=6 && a3<=20) {
				System.out.println("weird");
			}else {
				System.out.println("not weird");
			}
		}
	}
}
