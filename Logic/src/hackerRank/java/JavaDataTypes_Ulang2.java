package hackerRank.java;

import java.util.Scanner;

public class JavaDataTypes_Ulang2 {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn2 = new Scanner(System.in);
		
		//var utk loop
		int n2 = scn2.nextInt();
		
		//looping
		for (int j = 0; j < n2; j++) {
			
			try {
				long x2 = scn2.nextLong();
				System.out.println("merupakan : ");
				
				if(x2>=-128 && x2<=127)
					System.out.println("* byte");
				if(x2>=(Math.pow(2,15)*-1) && x2<=Math.pow(2,15)-1) 
					System.out.println("* short");
				if(x2>=(Math.pow(2,31)*-1) && x2<=Math.pow(2,31)-1) 
					System.out.println("* int");
				if(x2>=(Math.pow(2,63)*-1) && x2<=Math.pow(2,63)-1)
					System.out.println("* long");
			}
			catch (Exception e2) {
				System.out.println(scn2.next()+" can't be fitted anywhere.");
			}
		}
	}
}
