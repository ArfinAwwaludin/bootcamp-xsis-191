package hackerRank.java;

import java.util.Scanner;

public class JavaDataTypes_Ulang3 {
	//main method
	public static void main(String[] args) {
		//instansiai
		Scanner scn3 = new Scanner(System.in);
		
		//var utk loop
		int n3 = scn3.nextInt();
		
		//looping
		for (int k = 0; k < n3; k++) {
			try {
				long x3 = scn3.nextLong();
				System.out.println("merupakan : ");
				
				if(x3>=-128 && x3<=127)
					System.out.println("* byte");
				if(x3>=(Math.pow(2,15)*-1) && x3<=Math.pow(2,15)-1) 
					System.out.println("* short");
				if(x3>=(Math.pow(2,31)*-1) && x3<=Math.pow(2,31)-1) 
					System.out.println("* int");
				if(x3>=(Math.pow(2,63)*-1) && x3<=Math.pow(2,63)-1)
					System.out.println("* long");
			}
			catch (Exception e3) {
				System.out.println(scn3.next()+" can't be fitted anywhere.");
			}
			
		}
	}
}
