package hackerRank.java;

import java.util.Scanner;

public class JavaIfElse_Ulang2 {
	//membuat fungsi main
	public static void main(String[] args) {
		//memanggil scanner
		Scanner scn2 = new Scanner(System.in);
		
		//membuat variabel utk inputan
		int a2 = scn2.nextInt();
		
		//program 
		if(a2%2==1) {
			System.out.println("weird");
		}else {
			if(a2>=2 && a2<=5) {
				System.out.println("not weird");
			}else if(a2>=6 && a2<=20) {
				System.out.println("weird");
			}else {
				System.out.println("not weird");
			}
		}
		
	}
}
