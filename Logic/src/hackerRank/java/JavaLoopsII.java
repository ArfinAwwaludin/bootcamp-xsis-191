package hackerRank.java;

import java.util.Scanner;

public class JavaLoopsII {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn1 =  new Scanner(System.in);
		//variabel utk banyak looping
		int t1 = scn1.nextInt();
		//looping 1
        for(int i = 0; i < t1; i++){
            //variabel utk looping 2
        	int a1 = scn1.nextInt();
            int b1 = scn1.nextInt();
            int n1 = scn1.nextInt();
            
            //looping 2
            for(int j = 0; j < n1; j++){
            	a1 = a1+b1;
                System.out.print(a1+ " ");
                b1 = 2*b1;
            }
            System.out.println();
        }
	}
}
