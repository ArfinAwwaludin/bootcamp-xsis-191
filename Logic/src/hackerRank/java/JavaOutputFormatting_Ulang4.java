package hackerRank.java;

import java.util.Scanner;

public class JavaOutputFormatting_Ulang4 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn4 = new Scanner(System.in);
		
		//program
		System.out.println("===================");
		for (int k = 0; k < 3; k++) {
			//membuat variabel utk inputan
			String a4 = scn4.next();
			int b4 = scn4.nextInt();
			System.out.printf("%-15s%03d%n",a4,b4);
		}
		System.out.println("====================");
	}
}
