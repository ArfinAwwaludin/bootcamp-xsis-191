package hackerRank.java;

import java.util.Scanner;

public class JavaStdInAndStdOutII_Ulang2 {
	//membuat main method
	public static void main(String[] args) {
		//memanggil scanner/instansiasi
		Scanner scn2 = new Scanner(System.in);
		
		//variabel utk inputan
		int i2 = scn2.nextInt();
		Double d2 = scn2.nextDouble();
		String s2 = scn2.next();
		
		//menampilkan inputan
		System.out.println(s2);
		System.out.println(d2);
		System.out.println(i2);
	}
}
