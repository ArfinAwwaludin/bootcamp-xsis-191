package hackerRank.java;

import java.util.Scanner;

public class JavaOutputFormatting_Ulang5 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn5 = new Scanner(System.in);
		
		//program
		System.out.println("=============================");
		for (int l = 0; l < 3; l++) {
			//variabel utk inputan
			String a5 = scn5.next();
			int b5 = scn5.nextInt();
			System.out.printf("%-15s%03d%n", a5,b5);
		}
		System.out.println("=============================");
	}

}
