package hackerRank.java;

import java.util.Scanner;

public class JavaLoopsI_Ulang2 {
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn2 = new Scanner(System.in);
		
		//variabel utk inputan
		int a2 = scn2.nextInt();
		
		//program loop
		for(int j=0; j<10; j++) {
			int hasil2 = a2*j;
			System.out.println(a2+ " x " +j+ " = " +hasil2);
		}
	}
}
