package hackerRank.java;

import java.util.Scanner;

public class JavaEndOfFile_Ulang3 {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn3 = new Scanner(System.in);
		
		//looping
		for (int k = 0; scn3.hasNext(); k++) {
			String kata3 = scn3.nextLine();
			System.out.print(k+ " "+ kata3+ "\n");
		}
	}
}
