package hackerRank.java;

import java.util.Scanner;

public class JavaIntToString_Ulang4 {
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn4 = new Scanner(System.in);
		
		//var utk input
		int n4 = scn4.nextInt();
		
		//int to string
		String s4 = Integer.toString(n4);
		
		//if else statement
		if (n4 == Integer.parseInt(s4)) {
			System.out.println("good job");
		} else {
			System.out.println("wrong answer");
		}
	}
}
