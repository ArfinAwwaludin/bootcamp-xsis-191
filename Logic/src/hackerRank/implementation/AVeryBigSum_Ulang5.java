package hackerRank.implementation;

public class AVeryBigSum_Ulang5 {
	//membuat method
	static long aVeryBigSum5 (long[] arr5) {
		//assignment
		long a5 = 0;
		
		//program
		for (int m = 0; m < arr5.length; m++) {
			a5 = a5 + arr5[m];
		}
		//return pada method
		return a5;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		long[] arr5 = {4232,23525,564576,4325346,34};
		
		//var utk nampung hasil(a5), manggil method
		long a5 = aVeryBigSum5(arr5);
		
		//nampilin hasil
		System.out.println(a5);
	}
}
