package hackerRank.implementation;

import java.util.Scanner;

public class SolveMeFirst_Ulang5 {
	//membuat method
	static int solveMeFirst5(int a5, int b5) {
		return a5+b5;
	}
	
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn5 = new Scanner(System.in);
		
		//var utk inputan
		int a5 = scn5.nextInt();
		int b5 = scn5.nextInt();
		
		//var utk nampung hasil
		int sum5 = solveMeFirst5(a5, b5);
		
		//nampilin hasil 
		System.out.println(sum5);
		
	}

}
