package hackerRank.implementation;

import java.util.Arrays;

public class GradingStudents_Ulang3 {
	//membuat method
	static int[] gradingStudents3(int[]grades3) {
		//program
		for (int k = 0; k < grades3.length; k++) {
			if (k>=38) {
				if ((5-grades3[k] % 5) < 3) {
					grades3[k] = grades3[k] + (5-grades3[k] % 5);
				}
			}
		}
		//return dr method
		return grades3;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int [] grades3 = {33,56,78,44};
		
		//var nampung hasil
		int[] hasil3 = gradingStudents3(grades3);
		
		//nampilin hasil
		System.out.println(Arrays.toString(hasil3));
	}
}
