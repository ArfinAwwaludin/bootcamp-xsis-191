package hackerRank.implementation;

import java.util.Scanner;

public class StairCase_Ulang5 {
	//buat void method
	static void stairCase5(int n5) {
		for(int i=0; i<n5; i++) {
			if(i==n5){
                for(int j=1; j<=n5; j++){
                    if(i+j>=n5 ){
                        System.out.print("#");
                    }else{
                        System.out.print("");
                    }
                }
            } else {
                for(int j=1; j<=n5; j++){
                    if(i+j>n5){
                        System.out.print("#");
                    }else{
                        System.out.print(" ");
                    }
                }
           }
			System.out.println();
		}
	}
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn5 = new Scanner(System.in);
		//var utk input
		int n5 = scn5.nextInt();
		//manggil void method
		stairCase5(n5);
	}
}
