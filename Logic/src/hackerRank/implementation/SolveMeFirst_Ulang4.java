package hackerRank.implementation;

import java.util.Scanner;

public class SolveMeFirst_Ulang4 {
	//membuat method
	static int solveMeFirst4(int a4, int b4) {
		return a4+b4;
	}
	
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn4 = new Scanner(System.in);
		
		//var utk inputan
		int a4 = scn4.nextInt();
		int b4 = scn4.nextInt();
		
		//var utk nampung hasil & manggil method
		int sum4 = solveMeFirst4(a4, b4);
		
		//nampilin hasil
		System.out.println(sum4);
	}
}
