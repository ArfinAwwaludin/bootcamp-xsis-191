package hackerRank.implementation;

public class AVeryBigSum_Ulang4 {
	//membuat method
	static long aVeryBigSum4(long[] arr4) {
		//assignment
		long a4 = 0;
		
		//program
		for (int l = 0; l < arr4.length; l++) {
			a4 = a4 + arr4[l];
		}
		
		//return pada method
		return a4;
	}
	//main method
	public static void main(String[] args) {
		//inputan
		long[] arr4 = {88, 6678, 99827, 445633};
		
		//var utk nampung hasil(a4), manggil method
		long a4 = aVeryBigSum4(arr4);
		
		//nampilin hasil
		System.out.println(a4);
	}
}
