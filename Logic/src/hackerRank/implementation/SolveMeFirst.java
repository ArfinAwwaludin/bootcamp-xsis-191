package hackerRank.implementation;

import java.util.Scanner;

public class SolveMeFirst {
	
	//membuat method int dgn nama solveMeFIrst dan dgn parameter int a dan int b
	static int solveMeFirst(int a1, int b1) {
		//membuat return a+b karena method ini memiliki return value
		return a1+b1;
	}
	
	//membuat fungsi main
	public static void main(String[] args) {
		//memanggil scanner
		Scanner scn1 = new Scanner(System.in);
		//membuat variabel untuk inputan
		int a1 = scn1.nextInt();
		int b1 = scn1.nextInt();
		
		//membuat variabel utk menampung hasil operasi dan memanggil method
		int  sum1 = solveMeFirst(a1, b1);
		
		//menampilkan hasil operasi dari method ke console
		System.out.println(sum1);
		
	}

}
