package hackerRank.implementation;

public class DiagonalDifference_Ulang5 {
	//bikin method
	static int diagonalDifference5 (int[][] arr5) {
		//assignment nilai awal
		int a5 = 0;
		int b5 = 0;
		
		//program looping
		for (int m = 0; m < arr5.length; m++) {
			a5 += arr5[m][m];
			b5 += arr5[m][arr5.length-1-m];
		}
		
		//kondisi return
		if (b5>a5) {
			return b5-a5;
		}else {
			return a5-b5;
		}
	}
	//main method
	public static void main(String[] args) {
		//assignmnet input
		int [][] arr5 = {{2,4,5},{2,2,2,},{1,1,1}};
		
		//manggil method dan nampilin hasil
		System.out.println(diagonalDifference5(arr5));
	}
}
