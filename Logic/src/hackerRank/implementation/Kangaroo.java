package hackerRank.implementation;

public class Kangaroo {
	//membuat string method
	public static String kangaroo(int x1, int v1, int x2, int v2) {
		//assignment kondisi awal
		String a = "yes";
		
		//program percabangan
		if(x1<x2 && v1<v2) {
			a="no";
		}else {
			if((v1!=v2) && ((x2-x1)%(v1-v2))==0 ) {
				a = "yes";
			}else {
				a = "no";
			}
		}
		//return method
		return a;
	}

	//main method
	public static void main(String[] args) {
		//assignment inputan
		int[] a1 = {0,3,4,2};
		
		//variabel
		int x1 = a1[0];
		int v1 = a1[1];
		int x2 = a1[2];
		int v2 = a1[3];
		
		//menampilkan hasil dari pemanggilan method
		System.out.println(kangaroo(x1,v1, x2,v2));	
	}
}
