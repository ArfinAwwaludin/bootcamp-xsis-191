package hackerRank.implementation;

import java.util.Arrays;

public class GradingStudents_Ulang4 {
	//bikin method
	static int[] gradingStudents4(int[] grades4) {
		//program
		for (int l = 0; l < grades4.length; l++) {
			if (grades4[l] >= 38) {
				if ((5-grades4[l]%5) < 3) {
					grades4[l]=grades4[l]+(5-grades4[l]%5);
				}
			}
		}
		//return dr method
		return grades4;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int[] grades4 = {99,95,43,54};
		
		//var utk nampung hasil
		int [] hasil4 = gradingStudents4(grades4);
		
		//nampilin hasil
		System.out.println(Arrays.toString(hasil4));
	}
	
}
