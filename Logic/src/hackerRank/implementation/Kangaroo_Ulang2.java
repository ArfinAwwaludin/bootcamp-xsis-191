package hackerRank.implementation;

public class Kangaroo_Ulang2 {
	//membuat string method
	static String kangaroo(int x2_1, int v2_1, int x2_2, int v2_2) {
		//assignment kondisi awal
		String a2 = "yes";
		
		//program percabangan
		if((v2_1!=v2_2) && ((x2_2-x2_1)%(v2_1-v2_2))==0 ) {
			a2 = "yes";
		}else {
			a2 = "no";
		}
		//return method
		return a2;
	}
	//main method
	public static void main(String[] args) {
		//assignment inputan array 
		int [] b2 = {1,2,3,5};
		
		//variabel dr array int utk jadi int inputan method string
		int x2_1 = b2[0];
		int v2_1 = b2[1];
		int x2_2 = b2[2];
		int v2_2 = b2[3];
		
		//menampilkan hasil dan memanggil method
		System.out.println(kangaroo(x2_1, v2_1, x2_2, v2_2));
	}
}
