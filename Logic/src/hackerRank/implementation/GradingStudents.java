package hackerRank.implementation;

import java.util.Arrays;

public class GradingStudents {
	
	//membuat method int[] gradingStudents
	static int[] gradingStudents(int[] grades) {
		
		//program
		for(int i=0; i<grades.length; i++) {
			if(grades[i]>=38) {
				if((5-grades[i] % 5) < 3) {
					grades[i]=(grades[i]+(5-grades[i] % 5));
				}
			}
		}
		//return dr method 
		return grades;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int [] grades = {73,67,38,33};
		
		//var utk nampung hasil
		int[] hasil = gradingStudents(grades);
		
		//nampilin hasil
		System.out.println(Arrays.toString(hasil));
	}

}
