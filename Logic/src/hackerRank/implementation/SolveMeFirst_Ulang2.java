package hackerRank.implementation;

import java.util.Scanner;

public class SolveMeFirst_Ulang2 {

	//membuat method
	static int solveMeFirst2(int a2, int b2) {
		//membuat return value
		return a2+b2;
	}
	
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn2 = new Scanner(System.in);
		
		//variabel utk inputan
		int a2 = scn2.nextInt();
		int b2 = scn2.nextInt();
		
		//variabel utk menampung hasil
		int sum2 = solveMeFirst2(a2, b2);
		
		//mencetak hasil
		System.out.println(sum2);
	}
}
