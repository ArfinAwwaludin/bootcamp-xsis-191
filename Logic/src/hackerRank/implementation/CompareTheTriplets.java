package hackerRank.implementation;

import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets {
	
	static List<Integer> compareTheTriplets (List<Integer> a, List<Integer> b){
		List <Integer> result = new ArrayList();
        result.add(0);
        result.add(0);

        int nAlice = 0;
        int nBob = 0;

        for(int i=0; i<a.size();i++){
            if(a.get(i)>b.get(i)){
                nAlice++;
                result.set(0,nAlice);
            }
            if(a.get(i)<b.get(i)){
                nBob++;
                result.set(1,nBob);
            }
        }
    return result;
	}
	
	//main method
	public static void main(String[] args){
		
	}
}
