package hackerRank.implementation;

public class SimpleArraySum_Ulang2 {
	//membuat method
	static int simpleArraySum2(int[] arr2) {

		//assignment
		int result2 = 0;
		for (int j = 0; j < arr2.length; j++) {
			result2 = result2 + arr2[j];
		}
		//return method
		return result2;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int[] arr2 = {2,5,8,5,3};
		
		// var utk nampung hasil, manggil method
		int hasil2 = simpleArraySum2(arr2);
		
		//nampilin hasil
		System.out.println(hasil2);
	}
}
