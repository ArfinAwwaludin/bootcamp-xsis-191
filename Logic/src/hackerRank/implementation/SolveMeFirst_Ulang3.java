package hackerRank.implementation;

import java.util.Scanner;

public class SolveMeFirst_Ulang3 {
	//membuat method
	static int solveMeFirst3(int a3, int b3) {
		//membuat return value
		return a3+b3;
	}
	
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn3 = new Scanner(System.in);
		
		//var utk inputan
		int a3 = scn3.nextInt();
		int b3 = scn3.nextInt();
		
		//var utk nampung hasil & manggil method
		int sum3 = solveMeFirst3(a3, b3);
		
		//nampilin hasil
		System.out.println(sum3);
		
	}

}
