package hackerRank.implementation;

import java.util.Scanner;

public class StairCase {
	
	//membuat method void
	static void stairCase (int n1) {
		//program
		for(int i=0; i<n1; i++) {
			if(i==n1){
                for(int j=1; j<=n1; j++){
                    if(i+j>=n1 ){
                        System.out.print("#");
                    }else{
                        System.out.print("");
                    }
                }
            }else{
                for(int j=1; j<=n1; j++){
                    if(i+j>n1 ){
                        System.out.print("#");
                    }else{
                        System.out.print(" ");
                    }
                }
           }
			System.out.println();
		}
	}
	
	//membuat main method
	public static void main(String[] args) {
		//intansiasi
		Scanner scn1 = new Scanner(System.in);
		//var utk inputan
		int n1 = scn1.nextInt();
		//memanggil method stairCase
		stairCase(n1);
	}
	
}
