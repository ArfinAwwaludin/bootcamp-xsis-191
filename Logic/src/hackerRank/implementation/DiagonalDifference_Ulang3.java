package hackerRank.implementation;

public class DiagonalDifference_Ulang3 {
	//bikin method
	static int diagonalDifference3(int[][] arr3) {
		//assignment nilai awal 
		int a3 = 0;
		int b3 = 0;
		
		//program loopfing
		for (int k = 0; k < arr3.length; k++) {
			a3 += arr3[k][k];
			b3 += arr3[k][arr3.length-1-k];
		}
		if(b3>a3) {
			return b3-a3;
		}else {
			return a3-b3;
		}
	}
	
	//main method
	public static void main(String[] args) {
		//assignment inputan
		int[][] arr3 = {{4,6,4},{2,3,5},{2,7,6}};
		
		//manggil method & nampilin hasil
		System.out.println(diagonalDifference3(arr3));
	}
}
