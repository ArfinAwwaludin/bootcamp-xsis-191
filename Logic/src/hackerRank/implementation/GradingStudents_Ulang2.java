package hackerRank.implementation;

import java.util.Arrays;

public class GradingStudents_Ulang2 {
	//membuat method
	static int[] gradingStudents2(int[] grades2) {
		
		//program
		for (int j = 0; j < grades2.length; j++) {
			if (grades2[j]>=38) {
				if ((5-grades2[j] % 5) < 3) {
					grades2[j]=grades2[j]+(5-grades2[j] % 5);
				}
			}
		}
		//return dr method
		return grades2;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int[] grades2 = {76,69,33,30};
		
		//var utk nampung hasil
		int[] hasil = gradingStudents2(grades2);
		
		//nampilin hasil
		System.out.println(Arrays.toString(hasil));
	}
}
