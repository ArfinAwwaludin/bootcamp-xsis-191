package hackerRank.implementation;

public class AVeryBigSum_Ulang2 {
	//membuat method
	static long aVeryBigSum2 (long [] arr2) {
		//asignment
		long a2 = 0;
		
		//program
		for (int j = 0; j < arr2.length; j++) {
			a2 = a2 + arr2[j]; 
		}
		//return pada method
		return a2;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		long [] arr2 = {6,7,8,9,23};
		
		//nampung hasil
		long a2 = aVeryBigSum2(arr2);
		
		//nampilin hasil
		System.err.println(a2);
	}
}
