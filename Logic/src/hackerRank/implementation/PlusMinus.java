package hackerRank.implementation;

public class PlusMinus {
	
	//membuat method void dgn parameter int [] ar
	static void plusMinus(int[] arr) {
		//deklarasi variabel dgn tipe data float utk menampung akumulasi 
		float nPositive1 = 0;
		float nNegative1 = 0;
		float nZero = 0;
		
		//memulai looping utk mengecek tiap indeks array, memfilter bilangan dan mengakumulasi cacah bilangan
		for(int i=0; i<arr.length; i++) {
			//kondisi 1 utk memfilter bilangan negatif (arr[i]<0)
			if(arr[i]<0) {
				//mengitung cacah bilangan negatif
				nNegative1++;
			}
			//kondisi 2 utk memfilter bilangan positif (arr[i]<0)
			else if(arr[i]>0) {
				//mengitung cacah bilangan positif
				nPositive1++;
			}
			//kondisi 3 utk memfilter bilangan nol (arr[i]=0)
			else {
				//mengitung cacah bilangan nol
				nZero++;
			}
			
			//karena plusMinus merupakan method non return value, maka tidak dituliskan return
			//menampilkan hasil pada console
			System.out.println(nPositive1/arr.length);
			System.out.println(nNegative1/arr.length);
			System.out.println(nZero/arr.length);
		}	
	}
	
	//membuat main method utk memanggil dan menampilkan method plusMinus
	public static void main(String[] args) {
		//assignment utk inputan array
		int[] arr = {1,-2,3,0,1,1};
		
		//memanggil method plusMinus dan menampilkan hasil
		//plusMinus method void langsung dipanggil dibawah
		plusMinus(arr);
		
	}
}
