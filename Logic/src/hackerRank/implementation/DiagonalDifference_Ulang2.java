package hackerRank.implementation;

public class DiagonalDifference_Ulang2 {
	//bikin method
	static int diagonalDifference2(int[][] arr2) {
		//assignment 
		int a2 = 0;
		int b2 = 0;
		
		//program looping
		for (int j = 0; j < arr2.length; j++) {
			a2 += arr2[j][j];
			b2 += arr2[j][arr2.length-1-j];
		}
		
		//kondisi penguranga
		if(b2>a2) {
			return b2-a2;
		}else {
			return a2-b2;
		}
	}
	
	//main method
	public static void main(String[] args) {
		
		//assignment input
		int [][] arr2 = {{2,5,7},{5,5,7},{3,4,1}};

		//manggil method & nampilin hasil
		System.out.println(diagonalDifference2(arr2));
	}
}
