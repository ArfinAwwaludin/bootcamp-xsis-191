package hackerRank.implementation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MigratoryBird {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(4);
		list.add(3);
		list.add(2);
		list.add(1);
		list.add(3);
		list.add(4);
		System.out.println(migratoryBirds2(list));
	}
	
	static int migratoryBirds(List<Integer>arr) {
		Map<Integer, Integer> map = new HashMap();
		for(int i=0; i<arr.size(); i++) {
			if(map.containsKey(arr.get(i))) { 
				int n = map.get(arr.get(i));
				n++;
				map.put(arr.get(i), n);
			}else {
				map.put(arr.get(i), 1);
			}
		}
		
		int max = 0;
		int Key = 0;
		for (Map.Entry<Integer, Integer> item : map.entrySet()) {
			if(item.getValue()>max) {
				max = item.getValue();
				Key = item.getKey();
			}
			if(item.getValue()==max && item.getKey()<Key) {
				Key = item.getKey();
			}
		}
		return Key;
	}
	
	static int migratoryBirds2(List<Integer>arr) {
		Map<Integer, Integer> map = new HashMap();
		for (Integer item : arr) {
			if(map.containsKey(item)) { 
				int n = map.get(item);
				n++;
				map.put(item, n);
			}else {
				map.put(item, 1);
			}
		}
		
		int max = 0;
		int a = 0;
		for (Map.Entry<Integer, Integer> item : map.entrySet()) {
			if(item.getValue()>max) {
				max = item.getValue();
				a = item.getKey();
			}
			if(item.getValue()==max && item.getKey()<a) {
				a = item.getKey();
			}
		}
		return a;
	}

}
