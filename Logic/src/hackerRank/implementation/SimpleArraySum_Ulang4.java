package hackerRank.implementation;

public class SimpleArraySum_Ulang4 {
	//bikin method
	static int simpleArraySum4(int[] arr5) {
		//assignment 
		int result4 = 0;
		
		//program
		for (int l = 0; l < arr5.length; l++) {
			result4 = result4 + arr5[l];
		}
		//return method
		return result4;
	}
	
	//main method
	public static void main(String[] args) {
		//input
		int [] arr5 = {23,534,32434,345};
		
		//var nampung hasil, manggil method
		int hasil5 = simpleArraySum4(arr5);
		
		//nampilin hasil
		System.out.println(hasil5);
	}
}
