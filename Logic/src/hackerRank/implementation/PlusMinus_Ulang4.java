package hackerRank.implementation;

public class PlusMinus_Ulang4 {
	//membuat void method
	static void plusMinus4(int[] arr4) {
		//assignment utk nilai awal
		float nP4 = 0;
		float nN4 = 0;
		float nZ4 = 0;
		
		//program
		for (int k = 0; k < arr4.length; k++) {
			if (arr4[k]>0) {
				nP4++;
			}else if (arr4[k]>0) {
				nN4++;
			}else {
				nZ4++;
			}
		}
		//menampilkan hasil
		System.out.println(nP4/arr4.length);
		System.out.println(nN4/arr4.length);
		System.out.println(nZ4/arr4.length);
	}
	//main method
	public static void main(String[] args) {
		//input
		int[] arr4 = {1,2,3,4,0,0,-8,-9};
		//memanggil void method
		plusMinus4(arr4);
	}
}
