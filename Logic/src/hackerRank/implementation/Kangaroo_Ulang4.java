package hackerRank.implementation;

public class Kangaroo_Ulang4 {
	//membuat string method
	static String kangaroo4(int x4_1, int v4_1, int x4_2, int v4_2) {
		//assignment kondisi awal
		String a4 = "yes";
		
		//program percabangan
		if((v4_1!=v4_2) && ((x4_2-x4_1)%(v4_1-v4_2))==0 ) {
			a4 = "yes";
		}else {
			a4 = "no";
		}
		
		//return method
		return a4;
	}
	//main method
	public static void main(String[] args) {
		//assignment inputan array
		int [] b4 = {2,3,4,5};
		//variabel
		int x4_1 = b4[0];
		int v4_1 = b4[1];
		int x4_2 = b4[2];
		int v4_2 = b4[3];
		
		//menampilkan hasil
		System.out.println(kangaroo4(x4_1, v4_1, x4_2, v4_2));
	}
}
