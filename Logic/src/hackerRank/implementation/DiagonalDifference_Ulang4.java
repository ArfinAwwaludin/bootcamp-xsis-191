package hackerRank.implementation;

public class DiagonalDifference_Ulang4 {
	//bikin method
	static int diagonalDifference4(int [][] arr4) {
		//assignment nilai awal
		int a4 = 0;
		int b4 = 0;
		
		//program loop
		for (int l = 0; l < arr4.length; l++) {
			a4 += arr4[l][l];
			b4 += arr4[l][arr4.length-1-l];
		}
		
		//kondisi return utk pengurangan
		if (b4>a4) {
			return b4-a4;
		}else {
			return a4-b4;
		}
	}

	//main method
	public static void main(String[] args) {
		//assignment input
		int [][] arr4 = {{0,3,2},{1,4,7},{3,5,3}};
		
		//nampilin hasil dan manggil method
		System.out.println(diagonalDifference4(arr4));
	}
}
