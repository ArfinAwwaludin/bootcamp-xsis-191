package hackerRank.implementation;

import java.util.Scanner;

public class StairCase_Ulang3 {
	//membuat void main
	static void stairCase3(int n3) {
		for(int i=0; i<n3; i++) {
			if(i==n3){
                for(int j=1; j<=n3; j++){
                    if(i+j>=n3 ){
                        System.out.print("#");
                    }else{
                        System.out.print("");
                    }
                }
            } else{
                for(int j=1; j<=n3; j++){
                    if(i+j>n3 ){
                        System.out.print("#");
                    }else{
                        System.out.print(" ");
                    }
                }
           }
			System.out.println();
		}
	}
	
	//main method
	public static void main(String[] args) {
		//instansiasi 
		Scanner scn3 = new Scanner(System.in);
		//var utk inputan
		int n3 = scn3.nextInt();
		//manggil vaoid main
		stairCase3(n3);
	}

}
