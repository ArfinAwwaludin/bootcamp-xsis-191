package hackerRank.implementation;

public class Kangaroo_Ulang5 {
	//membuat string method
	static String kangaroo5(int x5_1, int v5_1, int x5_2, int v5_2) {
		//assignment kondisi awal
		String a5 = "yes";
		
		//program percabangan
		if((v5_1!=v5_2) && ((x5_2-x5_1)%(v5_1-v5_2))==0 ) {
			a5 = "yes";
		}else {
			a5 = "no";
		}
		//return method
		return a5;
	}
	
	//main method
	public static void main(String[] args) {
		//assignment inputan array
		int[] b5 = {2,5,8,9};
		
		//variabel
		int x5_1 = b5[0];
		int v5_1 = b5[1];
		int x5_2 = b5[2];
		int v5_2 = b5[3];
		
		//menampilkan hasil
		System.out.println(kangaroo5(x5_1, v5_1, x5_2, v5_2));
	}
}
