package hackerRank.implementation;

public class SimpleArraySum_Ulang5 {
	//membuat method
	static int simpleArraySum5(int[] arr5) {
		//assignment
		int result5 = 0;
		
		//program
		for (int m = 0; m < arr5.length; m++) {
			result5 = result5 + arr5[m];
		}
		//return of method
		return result5;
	}
	
	//main method
	public static void main(String[] args) {
		//input
		int[] arr5 = {64,234,435,46};
		
		//var nampung hasil, manggil method
		int hasil5 = simpleArraySum5(arr5);
		
		//nampilin hasil
		System.out.println(hasil5);
	}
}
