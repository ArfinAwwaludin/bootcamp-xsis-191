package hackerRank.implementation;

public class AppleAndOrange {
	//membuat void method
	static void countApplesAndOrange (int s, int t, int a, int b, int[] apples, int[] oranges) {
		
		int cApple = 0;
		for(int x=0; x<apples.length; x++){
            int jarak = a+apples[x];
            
            //jika jarak antara s & t
            if(jarak >=s && jarak <=t){
                cApple++;
            }
        }

        int cOrange = 0;
        for(int y=0; y<oranges.length; y++){
            int jarak = b+oranges[y];
            if(jarak >= s && jarak <= t){
                cOrange++;
            }
        }
        
        System.out.println(cApple);
        System.out.println(cOrange);
	}
	//main method
	public static void main(String[] args) {
		//inputan
		int s = 7;
		int t = 11;
		int a = 9;
		int b = 1;
		int[] apples = {4,2};
		int[] oranges = {3,6};
		
		//memanggil void method
		countApplesAndOrange(s, t, a, b, apples, oranges);
		
	}
}
