package hackerRank.implementation;

import java.util.ArrayList;
import java.util.List;

public class BirthdayChocolate {

	static int birthdayChocolate(List<Integer> s, int d, int m) {
		
		int a = 0;
		for (int i = 0; i <= s.size()-m ; i++) {
			int total = 0;
			
			for (int j = 0; j < m; j++) {
				total = total + s.get(i+j);
			}
			
			if(total==d) {
				a++;
			}
		}
		return a;
	}
	
	
	public static void main(String[] args) {
		int d = 3;
		int m = 2;
		List<Integer> s = new ArrayList<Integer>();
		s.add(1);
		s.add(2);
		s.add(1);
		s.add(3);
		s.add(2);
		
		System.out.println(birthdayChocolate(s, d, m));

	}

}
