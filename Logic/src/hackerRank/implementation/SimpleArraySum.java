package hackerRank.implementation;

public class SimpleArraySum {

	//membuat method
	static int simpleArraySum(int[] ar) {

		int result = 0;

		for(int i=0; i<ar.length; i++) {
			result = result + ar[i];
		}
		return result;
	}
	
	//membuat fungsi main
	public static void main(String[] args) {
		//assignment dengan tipe data data array, identifier ar dan inputan manual
		int [] ar = {1,2,3,4,5,6};
		
		//membuat variabel utk menampung dan memanggil method
		int a1 = simpleArraySum(ar);
		
		//menampilkan hasil pada console
		System.out.println(a1);
	}
}
