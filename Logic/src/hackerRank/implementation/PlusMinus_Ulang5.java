package hackerRank.implementation;

public class PlusMinus_Ulang5 {
	//membuat void method
	static void plusMinus5 (int[] arr5) {
		//assignment utk nilai awal
		float nP5 = 0;
		float nN5 = 0;
		float nZ5 = 0;
		
		//program
		for (int l = 0; l < arr5.length; l++) {
			if (arr5[l]>0) {
				nP5++;
			}if (arr5[l]<0) {
				nN5++;
			} else {
				nZ5++;
			}
		}
		//menampilkan hasil
		System.out.println(nP5/arr5.length);
		System.out.println(nN5/arr5.length);
		System.out.println(nZ5/arr5.length);
	}
	//main method
	public static void main(String[] args) {
		//input
		int[] arr5 = {1,2,3,4,5,6,7,0,-4};
		//manggil void method
		plusMinus5(arr5);
	}
}
