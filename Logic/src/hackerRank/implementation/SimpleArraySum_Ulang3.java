package hackerRank.implementation;

public class SimpleArraySum_Ulang3 {
	//membuat method
	static int simpleArraySum3(int[] arr3) {
		//assignment
		int result3 = 0;
		
		//program
		for (int k = 0; k < arr3.length; k++) {
			result3 = result3 + arr3[k];
		}
		//return pada method
		return result3;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int[] arr3 = {4,6,7,4,3,3,6};
		
		//var utk nampung hasil
		int hasil3 = simpleArraySum3(arr3);
		
		//nampilin hasil
		System.out.println(hasil3);
	}
}
