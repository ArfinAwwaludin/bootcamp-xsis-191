package hackerRank.implementation;

public class PlusMinus_Ulang3 {
	//bikin void method
	static void plusMinus3(int[] arr3) {
		//assignment nilai awal
		float nP3 = 0;
		float nN3 = 0;
		float nZ3 = 0;
		
		//program
		for (int j = 0; j < arr3.length; j++) {
			if (arr3[j]>0) {
				nP3++;
			}if (arr3[j]<0) {
				nN3++;
			} else {
				nZ3++;
			}
		}
		//nampilin hasil perbandingan
		System.out.println(nP3/arr3.length);
		System.out.println(nN3/arr3.length);
		System.out.println(nZ3/arr3.length);
	}
	
	//main method
	public static void main(String[] args) {
		//input
		int[] arr3 = {3,0,0,6,7,-4};
		
		//manggil void method
		plusMinus3(arr3);
	}
}
