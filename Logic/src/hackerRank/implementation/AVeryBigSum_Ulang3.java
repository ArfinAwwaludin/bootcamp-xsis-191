package hackerRank.implementation;

public class AVeryBigSum_Ulang3 {
	//membuat method
	static long aVeryBigSum3 (long[] arr3) {
		//asignment
		long a3 = 0;
		
		//program
		for (int k = 0; k < arr3.length; k++) {
			a3 = a3 + arr3[k];
		}
		//return pada method
		return a3;
	}
	//main method
	public static void main(String[] args) {
		//inputan
		long[] arr3 = {56, 75, 45, 66};
		//var utk nampung hasil
		long a3 = aVeryBigSum3(arr3);
		//nampilin hasil
		System.out.println(a3);
	}
}
