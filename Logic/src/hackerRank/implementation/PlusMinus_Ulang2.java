package hackerRank.implementation;

public class PlusMinus_Ulang2 {
	//bikin void method
	static void plusMinus2(int[] arr2) {
		//assignment nilai awal
		float nP2 = 0;
		float nN2 = 0;
		float nZ2 = 0;
		
		//prgram
		for (int i = 0; i < arr2.length; i++) {
			if (arr2[i] > 0) {
				nP2++;
			}else if (arr2[i] < 0) {
				nN2++;
			}else {
				nZ2++;
			}
		}

		//nampilin hasil perbandingan
		System.out.println(nP2/arr2.length);
		System.out.println(nN2/arr2.length);
		System.out.println(nZ2/arr2.length);
		
	}
	//main method
	public static void main(String[] args) {
		//input
		int [] arr2 = {2,5,-7,6,0,0,-5};
		
		//manggil void method utk nampilin hasil
		plusMinus2(arr2);
	}
}
