package hackerRank.implementation;

public class Kangaroo_Ulang3 {
	//membuat string method
	static String kangaroo3(int x3_1, int v3_1, int x3_2, int v3_2) {
		//assignment kondisi awal
		String a3 = "yes";
		
		//program percabangan
		if((v3_1!=v3_2) && ((x3_2-x3_1)%(v3_1-v3_2))==0 ) {
			a3 = "yes";
		}else {
			a3 = "no";
		}
		
		//return method
		return a3;
	}
	//main method
	public static void main(String[] args) {
		//assignment inputan array
		int [] b3 = {6,6,7,9};
		//variabel
		int x3_1 = b3[0];
		int v3_1 = b3[1];
		int x3_2 = b3[2];
		int v3_2 = b3[3];
		
		//menampilkan hasil
		System.out.println(kangaroo3(x3_1, v3_1, x3_2, v3_2));
	}
}
