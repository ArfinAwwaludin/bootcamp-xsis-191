package hackerRank.implementation;

public class DiagonalDifference {
	//membuat method diagonalDifference
	static int diagonalDifference(int[][] arr) {
		
		//assignment nilai awal
		int a1 = 0;
		int b1 = 0;

		//program looping
		for(int i=0; i<arr.length; i++) {
			a1 += arr[i][i];
			b1 += arr[i][arr.length-1-i];
		}
		
		//kondisi pengurangan
		if(b1>a1) {
			return b1-a1;
		}else {
			return a1-b1;
		}
	}
	
	//membuat main method
	public static void main(String[] args) {
		//assignment input
		int [][] arr = {{16,29,3},{91,2,3},{11,2,2}};
		
		//memamnggil method 
		System.out.println(diagonalDifference(arr));
	}
}
