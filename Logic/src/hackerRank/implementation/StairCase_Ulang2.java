package hackerRank.implementation;

import java.util.Scanner;

public class StairCase_Ulang2 {
	//membuat mvoid method
	static void stairCase2(int n2) {
		//program
		for(int i=0; i<n2; i++) {
			if(i==n2){
                for(int j=1; j<=n2; j++){
                    if(i+j>=n2 ){
                        System.out.print("#");
                    }else{
                        System.out.print("");
                    }
                }
            }else{
                for(int j=1; j<=n2; j++){
                    if(i+j>n2 ){
                        System.out.print("#");
                    }else{
                        System.out.print(" ");
                    }
                }
           }
			System.out.println();
		}
	}
	//membuat main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn2 = new Scanner(System.in);
		//var utk input
		int n2 = scn2.nextInt();
		//manggil void method utk nampilin hasil
		stairCase2(n2);
	}
	
}
