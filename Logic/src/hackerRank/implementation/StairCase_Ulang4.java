package hackerRank.implementation;

import java.util.Scanner;

public class StairCase_Ulang4 {
	//bikin void method
	static void stairCase4(int n4) {
		//program
		for(int i=0; i<n4; i++) {
			if(i==n4){
                for(int j=1; j<=n4; j++){
                    if(i+j>=n4 ){
                        System.out.print("#");
                    }else{
                        System.out.print("");
                    }
                }
            }else{
                for(int j=1; j<=n4; j++){
                    if(i+j>n4){
                        System.out.print("#");
                    }else{
                        System.out.print(" ");
                    }
                }
           }
			System.out.println();
		}
	}
	
	//main method
	public static void main(String[] args) {
		//instansiasi
		Scanner scn4 = new Scanner(System.in);
		//var utk inputan
		int n4 = scn4.nextInt();
		//manggil void method
		stairCase4(n4);
	}
}
