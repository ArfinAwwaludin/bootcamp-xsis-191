package hackerRank.implementation;

import java.util.Arrays;

public class GradingStudents_Ulang5 {
	//membuat method
	static int[] gradingStudents5(int [] grades5) {
		//program
		for (int m = 0; m < grades5.length; m++) {
			if (grades5[m]>=38) {
				if((5-grades5[m]%5) < 3) {
					grades5[m] = grades5[m] + (5-grades5[m]%5);
				}
			}
		}
		//return method
		return grades5;
	}
	
	//main method
	public static void main(String[] args) {
		//inputan
		int [] grades5 = {44,66,77,88};
		
		//var nampung hasil
		int[] hasil5 = gradingStudents5(grades5);
		
		//nampilin hasil
		System.out.println(Arrays.toString(hasil5));
	}
}
