package hackerRank.implementation;

public class AVeryBigSum {
	//membuat method
	static long aVeryBigSum(long [] ar) {
		//assignment
		long a=0;
		
		//program
		for(int i=0; i<ar.length; i++) {
			a=a+ar[i];
		}
		//return pada method
		return a;
	}
	
	public static void main(String[] args) {
		//inputan
		long[] ar = {1,2,3,4,5};
		//var nampung hasil
		long a = aVeryBigSum(ar);
		//nampilin hasil
		System.out.println(a);
		
	}
	
	//array, OOP, method
	//hackerrank pengulangan min 5, tiap soal jadi kelas
	//perbaris bikin komentar
	//setiap ulangan variabelnya beda
	
}